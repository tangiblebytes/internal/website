---
title: "Writing Golang"
date: 2020-05-15T13:14:41+01:00
draft: false
tags: ["golang"]
---

I've really enjoyed learning [Golang](https://golang.org/) lately, the [tour](https://tour.golang.org/welcome/1) is a great place to start and I found using [VSCode](https://code.visualstudio.com/) as an IDE really helped with automatic formatting and highlighting of errors.

Best things about Go so far for me are

<!--more-->

1.  Simple language structure
1.  Runs fast
1.  Easy to distribute my code (just one binary)
1.  Lots of good libraries available
1.  Incredibly small containers

The language structure is really easy to pick up - with more consistency than PHP and much less boilerplate than Java

I've worked mostly in PHP so I'm switching from an interpreted to a compiled language - of course occasionally I had trouble debugging because I forgot to recompile.

But mostly I was impressed with the much faster response time of web pages, and caught more bugs at compile time without leaving the IDE.

PHP and Java have both developed package managers that help with dependencies - Go has a less mature process for this but also it is less critical because libraries are only a build dependency - they are compiled into the release artefact so you don't have any dependency management outside of compiling.

{{< aside >}}

#### A note on Go Modules

[Go Modules](https://blog.golang.org/using-go-modules) are fairly new

Some examples of how to write golang code don't cover modules

Make sure you read https://golang.org/doc/code.html

Do follow the steps on initialising a module so that you can better track your dependencies

{{< /aside >}}

I found good existing libraries to do everything I needed.

Some (notably golang docker) didn't adhere to semver which made tracking versions difficult see this issue for more detail https://github.com/moby/moby/issues/39056

I'm a little concerned how easy it would be to update and rebuild my code after a period of time - possibly from a different dev environment - and intent to look into this more thoroughly as I have had bad experiences in the past caused by library changes over time in other languages.

But because what I use on the server is a binary - once it's built I don't have to worry about libraries again.

I used https://jeremylong.github.io/DependencyCheck/ to make sure I stayed up to date and while the Golang analyzer is experimental I found it worked well (though I had to add a manual suppression for docker)

Because Go creates a single binary without depending on external libraries, and typically Go runs it's own server instead of depending on something like Apache -you can create a container that only has that binary. It will be tiny and has a super short startup time.

https://medium.com/tourradar/lean-golang-docker-images-using-multi-stage-builds-1015a6b4d1d1

This radically reduces Systems administration as there really isn't anything else to administer, secure, or update.
