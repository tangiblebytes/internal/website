---
title : "UK Geodata"
date : 2022-12-12T21:47:42Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - UK
 - Geography
 - Open Data

---
<div class="clearfix">

![](/uk-map-200.webp)
{.float-left}

I have been working with some of the data from The Office of National Statistics and Ordnance Survey.

If you want some lookup tables to find out how wards, districts/divisions, counties and so on all relate take a look here 

https://github.com/seanburlington/uk_geodata

</div>
<!--more-->

I would like to write a bit more about this - but honestly UK geography is so confusing and I can make sense of the data but am not ready to make a decent explanation yet.

# Some great resources

- [UK Election maps](https://www.ordnancesurvey.co.uk/election-maps/gb/)
- [National Statistics Geodata](https://geoportal.statistics.gov.uk/)
- [Ordnance Survey Open Data](https://www.ordnancesurvey.co.uk/business-government/tools-support/open-data-support)
- [Beginners Guide to UK Geography](https://geoportal.statistics.gov.uk/documents/a-beginners-guide-to-uk-geography-2021-v1-0-1/explore)
- [Paf (postcode) Programmers Guide](https://www.poweredbypaf.com/wp-content/uploads/2017/07/Latest-Programmers_guide_Edition-7-Version-6-1.pdf)


