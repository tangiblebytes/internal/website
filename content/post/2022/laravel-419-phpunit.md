---
title : "Laravel 419 Phpunit"
date : 2022-11-03T11:29:33Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - laravel
 - phpunit

---

A common problem with  [Laravel](https://laravel.com/) seems to be getting 419 errors.

If you have fixed these except for during testing : disable caching.

<!--more-->

## General 419 Error Causes

This error is supposed to occur when the users session has expired, or if someone is making requests to the site which originate elsewhere and may be malicious.

They can also occur if your cache is stale `php artisan cache:clear`

Or if storage folders don't have the right permissions

These all need to be writeable by the webserver user 

```bash
storage
vendor
bootstrap/caches
```

Be careful if the webserver is running as a different userID to the user running any `artisan` commands

## 419 Errors During Unit Testing 

Laravel automatically disables CSRF during unit testing 

https://laravel.com/docs/9.x/csrf#csrf-excluding-uris


> For convenience, the CSRF middleware is automatically disabled for all routes when running tests.


But - you have to clear the cache for this to work!

https://laravel.com/docs/9.x/testing#environment


> The testing environment variables may be configured in your application's phpunit.xml file, but make sure to clear your configuration cache using the config:clear Artisan command before running your tests!


Otherwise you may find that a lot of tests fail!

During development you are probably better off without any caching enabled

To clear and disable all caching run 

```bash
php artisan clear-compiled
php artisan auth:clear-resets
php artisan cache:clear
php artisan config:clear
php artisan debugbar:clear
php artisan event:clear
php artisan optimize:clear
php artisan queue:clear
php artisan route:clear
php artisan schedule:clear-cache
php artisan view:clear
```

Don't run any optimizations - the site will be slower but tests will work and any development changes will show up immediately.

