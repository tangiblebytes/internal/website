---
title : "See How a Docker Image was Built"
date : 2022-11-08T14:03:42Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - docker

---

I had a docker image and couldn't find the Dockerfile but wanted to see what was in it

```bash
docker image history --no-trunc --format="{{.CreatedBy}}" imagename
```

<!--more-->

It's not as good as the Dockerfile but shows a lot of what is going on.

Also a reminder that commands in a container aren't secret just because the image is in binary form.
