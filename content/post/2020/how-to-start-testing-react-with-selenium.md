---
title: "How to Start Testing React With Selenium"
date: 2020-11-26T21:24:43Z
#images : ["/2016/10/image.jpg"]
description: "The very first steps to setup Selenium testing."
#categories : ["category"]
draft: true
author: Sean Burlington
tags:
  - QA
  - React
  - Selenium
---

> If you didn't test it : it doesn't work.

Software development is complex and is rarely perfect - even less frequently is it perfect at the first attempt.

Only testing provides some assurance that the thing you developed mostly does what was needed.

Automated Integration testing

<!--more-->

In your project

```bash
npm install -D selenium-webdriver
```

Download the server
https://www.selenium.dev/downloads/

Start with

```bash
java -jar selenium-server-standalone-3.141.59.jar
```

Leave this running

Download the latest driver and put it on your path

https://github.com/mozilla/geckodriver/releases

Keep your selenium test files in the main project repo.

I put mine in `src/__tests__/selenium`

This way tests can be added/changed in the same branch as features or bugfixes.

Run your test

```bash
node src/__tests__/selenium/example.js
```
