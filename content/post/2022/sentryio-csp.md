---
title : "Monitoring CSP errors via Sentry.io"
date : 2022-01-19T20:53:44Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Monitoring
 - CSP
 - Sentry.io
 - Security

---

I love the extra security [CSP](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) brings - but it's still a bit new to me and I hadn't setup reporting because this is a static site and I didn't think I had anywhere easy to send the errors.


However I use [Sentry](https://sentry.io) for another project and realised that it offers [easy CSP reporting](https://docs.sentry.io/product/security-policy-reporting/).

<!--more-->


> Sentry provides the ability to collect information on Content-Security-Policy (CSP) violations, as well as Expect-CT and HTTP Public Key Pinning (HPKP)   
>   
> failures by setting the proper HTTP header which results in violation/failure to be sent to Sentry endpoint specified in report-uri.   
>    
>    The integration process consists of configuring the appropriate header with your project key’s Security Header endpoint found at Project Settings > Security Headers.

This is great because even my small, static, serverless site can have good monitoring - and if I make a mistake that affects some browser I will find out.
