---
title: "Support Internet Explorer to Save the World"
date: 2021-02-15T13:13:02Z
images: ["/2021/Internet_Explorer_9_icon.webp"]
description: "Supporting older technologies like Internet Explorer allows people to use older hardware, saving them money and reducing environmental impact."
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - low carbon
  - environment
  - web
  - Internet Explorer
---

![](/2021/Internet_Explorer_9_icon.webp)

Supporting old technologies is frustrating for web developers, we all want to use new features and the latest tools.

"Only" [5% of website traffic](https://news.softpedia.com/news/what-a-strange-year-internet-explorer-market-share-actually-increases-531438.shtml)
is coming from people using Internet Explorer

Even [Microsoft say people should upgrade](https://www.microsoft.com/en-gb/microsoft-365/windows/end-of-ie-support)

Supporting additional browsers costs time and money.

Why should we do it ?

<!--more-->

Firstly you have to consider your purpose and audience: not every site will make the same choice.

But here's why I think we should be very careful before dropping support.

## New Software Means New Hardware

If you make people upgrade their browser to access your site it may well mean replacing that hardware.

It's not like people are using IE on a brand new laptop - they probably didn't upgrade because they can't upgrade.

My old laptop is from Dell (one of the more eco-conscious manufacturers) and [it looks like](https://corporate.delltechnologies.com/en-us/social-impact/advancing-sustainability/sustainable-products-and-services/product-carbon-footprints.htm#tab0=0) my laptop carbon footprint is around about 300kg

The best way to reduce the impact of hardware - is to extend the life of it and here web developers have a part to play.

Use progressive enhancements but where possible support old software.

## Choice Is a Good Thing

I've been doing this long enough to remember the [First Browser War](<https://en.wikipedia.org/wiki/Browser_wars#First_Browser_War_(1995%E2%80%932001)>)
when there was a strong temptation to give up supporting [Netscape Navigator](https://en.wikipedia.org/wiki/Netscape_Navigator) and develop sites purely for Internet Explorer which was at the time dominant.

But we didn't : we supported it even when it had very little market share and by supporting public standards allowed diversity to continue in teh marketplace.

## 5% Is a Lot

For many businesses losing or gaining 5% is a huge deal. It's easy for us web developers to dismiss it as insignificant - but it probably isn't.

If 5% of your customers move to a competitor instead of upgrading - you might wish you'd put the effort into compatibility.

Imagine if 5% of requests to your server were producing errors - that would signify a significant problem.

## Which 5% Is It ?

It's not a random 5%

The people on old browsers will fall into a few groups

- Corporate mandated software

  Some companies enforce consistency for security reasons or to support in-house apps

  Be careful if these users are significant to your market.

- People with less money

  Some people just can't afford to upgrade, if you are selling luxury good this may not matter to you. But if you are in any way providing a public service you probably have a duty to make your service available to all.

- People using accessibility devices

  screen readers and physical adaptations are expensive and may cause people to hang on to hardware longer than you might expect. You may have moral and legal obligations top serve these protected groups.

## Summary

Every site is unique, and there are multiple competing priorities but think carefully before ditching support for something that is still in daily use by millions of people.
