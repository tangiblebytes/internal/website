---
title : "Windows 11 WSL2 and Docker Desktop"
date : 2024-08-07T22:19:48+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Windows
 - Linux
 - WSL
 - WSL2
 - Docker
 - Docker Desktop

---
<div class="clearfix">

I'm a long time Linux user, and a big fan of Docker, Kubernetes, VMs, and containers. 

While I love developing on Linux - I do sometimes find it frustrating that I don't have the same level of access to the Microsoft suite on Linux.

A while ago I had to use a Windows laptop for a few days and tried out using WSL - so most of the time I'm in Linux - I didn't entirely hate it so when it was time for an upgrade I decided to try the Windows route this time.

I've had my new machine for a little over a week now.

</div>

<!--more-->


## First thoughts

WSL is a lightweight and optimised VM that lets you run Linux (various distributions) on Windows, but unlike a regular VM this is more tightly integrated with the host.

You can run Linux apps from Windows and windows executables in Linux.

VSCode is especially well integrated.

Docker desktop is also tightly integrated so that I can interact with docker and local kubernetes resources both from Windows and Linux.

All my dev files exist inside of the Linux system, which is mounted in Windows too - but have Linux style file permissions. This means file operations are fast where my code runs but also accessible within Windows.



## Networking Voodoo

This is all a bit simpler on Linux - everything runs on localhost - and if you run docker you can expose ports on localhost too.

On WSL there is some networking magic so that localhost is shared between host and guest - I think this is mostly just NAT and maybe I have done something wong but I had trouble accessing a website running under WSL that needed host headers working properly.

I use `/etc/hosts` or `C:\Windows\System32\drivers\etc\hosts` as it is windows to configure the hostnames I need to resolve locally - I seem to need to use the WSL IP Address here instead of 127.0.0.1 

## Docker Desktop

It's great having a local kubernetes cluster (I used minikube on Linux).

The **Reset Kubernetes Cluster** button is really useful for testing build scripts because you can tear it all down and start clean again.

Note that docker automagically manages your `~/.kube/config` file and will rewrite this when restarting the cluster.

I'd renamed mine and the default one wasn't in my KUBECONFIG env variable so kubectl stopped working after a cluster reset till I fixed this.

Ingresses seem to work fine with hostnames pointing to 127.0.0.1

## Summary 

Well I haven't been using it long but it mostly seems to be the best of both worlds.

I am still developing on Linux - I get to use bash, apt, and snap for the things I care about.  

I have Windows for full integration with the workplace - and if the software is buggy nobody gets to blame "for using Linux"

There is an extra layer of complexity though - not all of which I understand - I hope nothing that will cause real problems

