---
title : "Golang Mysql Kubernetes Restarts"
date : 2024-01-02T15:08:28Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : true
author : Sean Burlington
tags :
 - Go
 - Golang
 - MySQL
 - Kubernetes

---

I have inherited an application written in Go, hosted in Kubernetes and connecting to MySQL.

It is experiencing restarts with no errors logged and I'm not sure why.


<!--more-->

## Are connections resilient ?

Testing locally with docker if I stop the MySQL service, or course I get an error in the Go app. 

When I start MySQL again the Go app works normally so the DB driver must be restoring the connection.

## Is the problem Idle Connection Timeouts

This app is low traffic - perhaps the database server drops the connections after too long idle ??