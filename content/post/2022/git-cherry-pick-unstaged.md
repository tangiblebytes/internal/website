---
title : "Git Cherry Pick Unstaged"
date : 2022-11-07T22:17:22Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - git
 - note-to-self

---

I wanted to borrow a commit from another branch

```bash
git cherry-pick -n some-commit
git reset
```
<!--more-->

This copies the code to my workspace but without the commit and unstaged 

So I can check it works OK without bringing the commit to my branch 