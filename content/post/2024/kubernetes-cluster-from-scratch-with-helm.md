---
title : "Kubernetes Cluster From Scratch With Helm"
date : 2024-08-07T22:32:15+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : true
author : Sean Burlington
tags :
 - Kubernetes
 - Helm


---
<div class="clearfix">

![](/kubernetes.png)
{.float-left}

Most of the time when I add something to our Kubernetes cluster at work I just need to run a helm chart, maybe add a namespace.

Recently I was testing by deploying to a new cluster and realised I needed a few things in the cluster.

</div>
<!--more-->

## Nginx Ingress

I found this tutorial useful in understanding the basics in a step by step way 

https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm

<!--
My experiments at 
https://gitlab.com/sciedsup/experiments/helm-kubernetes-ingress
-->

But really all you need is 

```bash 
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```


> On most systems, if you don't have any other service of type LoadBalancer bound to port 80, the ingress controller will be assigned the EXTERNAL-IP of localhost, which means that it will be reachable on localhost:80.

from the [Kubernetes Quick Start Guide](https://kubernetes.github.io/ingress-nginx/deploy/#quick-start)

You can verify the external ip using 

`kubectl get service --namespace ingress-nginx ingress-nginx-controller --output wide --watch`


## Hello World

With an Ingress deployed you can install a Hello World site 

This example on github is useful 

https://github.com/paulbouwer/hello-kubernetes

I have modified a version to use the ingress controller just installed 

https://gitlab.com/tangiblebytes/hello-kubernetes

The following assumes you are using Kubernetes on docker-desktop  - take care when copying this code and adjust if needed.

```bash
echo "127.0.0.1    hello-world.docker.desktop" | sudo tee -a /etc/hosts
git clone git@gitlab.com:tangiblebytes/hello-kubernetes.git
cd hello-kubernetes/deploy/helm
helm install --create-namespace --namespace hello-kubernetes hello-world ./hello-kubernetes
```

You should now be able to access your new site on 

http://hello-world.docker.desktop/


--- make this a new post

## Dashboard

Installing the Kubernetes dashboard gives visual access to the system, it's nothing you cant get without kubectl - but it's much easier to browse


https://github.com/kubernetes/dashboard?tab=readme-ov-file#installation

```bash
# Add kubernetes-dashboard repository
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
# Deploy a Helm Release named "kubernetes-dashboard" using the kubernetes-dashboard chart
helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --create-namespace --namespace kubernetes-dashboard
```

Wait a bit for it to install then forward the port to connect 

```bash 
kubectl -n kubernetes-dashboard port-forward svc/kubernetes-dashboard-kong-proxy 8443:443
```


## with admin user 
https://gitlab.com/sciedsup/support/infra/-/tree/main/dashboard/helm/k8s-dash


-- also a post on adding cert manager (does it work in kubernetes)

-- some time figure out monitoring