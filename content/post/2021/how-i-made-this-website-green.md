---
title: "How I Made This Website Green"
date: 2021-02-14T11:34:07Z
#images : ["/2016/10/image.jpg"]
description: "Choices made to make this website as low carbon as possible while balancing other needs."
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - low carbon
  - balance
  - architecture
  - hugo
  - firebase
  - this website
---

How did I balance all the other requirements of a website with the need to minimise energy use?

To make this site [cleaner than 98% of web pages tested](https://www.websitecarbon.com/website/tangiblebytes-co-uk/)

<!--more-->

## First I defined my Goals

My first step was to honestly consider the purpose of this site.

It is a low traffic blog, with fairly infrequent updates (average less than daily), no registered users.

I would quite like comments on the site but spam make this more work than it is worth.

I care about speed, security, accessibility, longevity, cost, and energy efficiency.

Design is important, although I am not selling design expertise so my main aim is to make it unobtrusive.

## Data Centre Energy Use

Data Centres are energy hungry places, they use power to run the computers and to keep the computers cool.

The systems can be more or less efficient, the energy used can be more or less Green, the resources can be allocated more efficiently, and how much system you use can be greater or less.

It's hard to know what happens in a data centre.

But you can controls some of it.

1. System efficiency

   This depends on the hosting provider you use and is the equivalent of choosing an airline with fuel efficient planes.

2. Green Energy

   Many hosting providers now use green energy based on renewable resources. Choosing a green provider can have a big impact.

3. Efficient Resource allocation

   In the early days of the web it was common to use physical servers - so you would have a whole computer running your website. During the night it might be completely idle, and because the server had to be powerful enough to handle busy times - most times it was underused.

   Various levels of virtualisation and auto-scaling have massively improved the efficiency of resource allocation so that most servers are much busier now and spend less time using electricity but just waiting.

4. Reduce how much you use

   More efficient code in a Drupal site will use less power, smarter caching strategies in WordPress will reduced the number of database queries made.

   The biggest reduction is to remove the web server and database entirely.

   Static Site Generators prepare pages in advance and only have to serve static files.

   If you don't need things like comments or shopping carts, and your site is updated less than say once an hour on average - this can be by far the most efficient way to serve a website with pages served directly from a CDN.

## Hugo

I use [Hugo](https://gohugo.io/) to create the pages, it allows me full control over page design, adding content is easy and regenerating the site takes less than a second.

Importantly to me Hugo is a well supported project delivered in a way that I am confident will never force me to run system updates or leave me having trouble updating my content.

## Hosting

AWS is amazing in the range of services they offer, but they have [the weakest position on energy use](https://sustainability.aboutamazon.com/environment/the-cloud?energyType=true) of the big providers.

Microsoft have a [really good position](https://news.microsoft.com/sustainability-by-the-numbers/en/renewable-energy) and when I looked into it they seemed the greenest.

Google have a [good position](https://cloud.google.com/sustainability/)

But energy efficiency isn't the only consideration.

[Google's Firebase](https://firebase.google.com/docs/hosting/) provides a fantastically easy to use hosting platform for static sites.

In fact it is a lot more than this with Cloud functions and their Firestore database also really easy to use.

The sheer ease of use won for me though, I can setup a new site in minutes, with a custom domain and free SSL certificate.

In fact Google offer a generous free tier for hosting and I have only paid a few pence for backup space (and that because I forgot to remove old backups).

## Design and Content

Making a text based website load fast and maintain accessibility is fairly easy.

I've written JavaScript applications elsewhere but here I decided not to use any JavaScript at all - after all I just want people to read stuff.

I've used a [Bootstrap](https://getbootstrap.com/) theme because it's fairly clean, looks modern enough, and is what I've used elsewhere. It probably isn't ideal here but there is a real benefit for me in re-using the same knowledge across projects.

Without any JavaScript, images, video, or audio, with fairly clean HTML, minification and a sensible cache policy - the page size is very small and there is very little processing needed on the browser (meaning that it doesn't flatten your battery).

Oh and I'd already decided to drop analytics as they aren't really that compatible with GDPR - really if we'd had to add cookie pop-ups to add analytics nobody would have done it.

Even with a much richer design, a site like this one can still be blazingly fast.

The real challenge is making complex interactive web applications stay fast and accessible - but that's another story for another post sometime.

## Compromise and balance

I've compromised in a few places

Hosting : I think Microsoft hosting is greener but now by much so I've used Google because it fits my technical needs better.

Design : I'm not going to claim this is the worlds best designed site, and I've still used more CSS than I need because I use a framework instead of rolling a custom theme.

Perfection : There are still some things on the TO DO list.

But overall I have a site that:

- I can update easily
- I am confident I can run for a long time
- is on a host where I can add dynamic features if I need them
- is highly secure with best practice headers in place
- has good accessibility
- is very cheap to run

and according to the Website Carbon Calculator is

[cleaner than 98% of web pages tested](https://www.websitecarbon.com/website/tangiblebytes-co-uk/)

Actually I think it is better than that : their methodology assumes that the energy used to generate a page, deliver it, and render it is proportional to the amount of data transferred. My site is designed to use minimal energy at every stage and doesn't use any energy hungry web processors or databases.

## What Choices Would You Make ?

There is more than one right answer and more than one way to measure success.

Maybe you need a shopping cart, a richer design, or integration with a legacy system?

This post is not about how you should make your decisions - only about how I made mine.

What I hope it shows is that by considering energy impacts it is possible to make a big difference.
