---
title : "Laravel Formatting with 'Pint'"
date : 2022-11-12T14:44:26Z
images : ["/laravel-logo-200.png"]
description : "How to make everyone happy and focussed on the important stuff by automating code formatting."
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Laravel
 - Coding
 - Format
 - Pint

---

![](/laravel-logo-200.png)
{.float-left}

Code reviews are a real opportunity to learn from each other - we each write code a little differently  and bring with us a range of experience. So much better to spend time on this than debating blank lines.

I'm also slightly dyslexic and find it hard to even see the issues some care passionately about.

I want to do the right thing for the team but going through code line by line and applying a strict set of rules  is not something I am good at or enjoy.

Computers are good at following rules though.



<!--more-->

Luckily for me I'm using Laravel and they recently added a code formatter 

[Laravel release 9.3.0](https://github.com/laravel/laravel/releases/tag/v9.3.0)

> Uses laravel/pint for styling by @nunomaduro in #5945

My project is actually on Laravel 9.2 but IU can still get iut I just need to 

`composer require laravel/pint --dev`

From here you can just run `pint` with the standard defaults but I wanted to adjust to the formatting rules my project follows.

So I added in `pint.json`

```json
{
    "preset": "laravel",

    "rules": {
        "binary_operator_spaces": {"default" : "align_single_space"}
    }
}
```

Check the underlying [PHP-CS-Fixer configuration](https://mlocati.github.io/php-cs-fixer-configurator/) for available rules.

Then `./vendor/bin/pint --test` to check for formatting issues 

and `./vendor/bin/pint` to fix them all 

You can get `pint` to show diff but I find it easier to just make the changes and then look at the diff in git.

I'd recommend fixing all existing formatting issues in one go when this is introduced, then run `pint` as part of the CI process so that all new code stays formatted as expected.

I'm sure `pint` isn't going to be as smart about formatting as some people can be - and you might not like all the rules it follows.

But the point surely is to free up developer brainpower for tasks that deliver business value - and to spend code review asking questions like "why have you done it this way?" "have you considered?" and "what if?".

Code reviews can be a really uplifting experience where we feel we have given something good to the project and learned from our peers.

Let's focus on writing good code - leave the formatting to the computer.




