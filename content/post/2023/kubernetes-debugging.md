---
title : "Kubernetes Debugging"
date : 2023-07-03T12:55:04+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Kubernetes
 - Debugging
 - Docker

---

Some quick notes on how to debug containers in Kubernetes

<div class="clearfix">

![](/logos/kubernetes-200.png)
{.float-left}


</div>
<!--more-->

## Get Events 


`kubectl events -n mynamespace`

or for all namespaces

`kubectl events -A`

Note that `kubectl events` gets events only for the _default_ namesapce




## View Logs 

Find the pod via 

`kubectl get pods -n mynamespace`

then for each pod of interest

`kubectl logs -n mynamesapce mypodname`

This gives whatever logs the container is generating - like the nginx log if it is an nginx container, or the nodejs output if you're running node - tec

NB you can follow the logs with `-f`



## Port forward 

If the ingress isn't setup - or isn't working you can test tge application locally by port forwarding 

`kubectl -n mynamesapce port-forward mypodname 1337:1337`

Giving the port numbers you want to forward from and to.

## Run shell 

Much like getting a shell on Docker you can get a shell on Kubernetes (if the container has a shell)

`kubectl exec --stdin --tty shell-demo -- /bin/sh`


## Ingress Controller logs

If you're trying to see why the ingress isn't working the nginx ogs may help

`kubectl get pods | grep nginx`

Take the pod name 

`kubectl logs -f  tlsproxy-ingress-nginx-controller-77b8fb5dfd-sx9bc`