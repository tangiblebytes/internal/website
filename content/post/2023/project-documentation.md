---
title : "Project Documentation"
date : 2023-01-05T14:48:55Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Documentation
 - Git
 - Wiki

---

Have you ever been involved in a project that has been running for months and now every time you have a discussion people say "we have done this already" ?

The problem is often that the discussions have not been documented - and the meetings need to be repeated.

So what are the best tools and processes to escape the meeting cycle and deliver quality?

<!--more-->

## Tools

Tools are only part of the solution - but the right tools can really help.

What makes the right tools will vary from team to team and project to project.

I'm used to software development projects with a mostly technical team.


### Trac

> [Trac](https://trac.edgewall.org/) is an enhanced wiki and issue tracking system for software development projects. 

I haven't used this tool in many years but I used to love it.

It contains 

- issue tracking
- wiki
- source code browser
- release tracker

The thing I loved about it was that these things cross reference 

So you can document a plan in the wiki - and add links to issues - which automatically get crossed off when the issue is closed. Usually the issue also relates to a code change which is also linked. So it becomes very easy to relate a business requirement to an issue to lines of code.

### Confluence / Jira

[Atlassian](https://www.atlassian.com/) have done an amazing job of creating very feature rich tools 

Jira and Confluence can cross reference nicely - if you use Bitbucket too then you get all the related data.

It does get very expensive in the Atlassian world.

While very powerful it is also very complex and needs time investing - but in return it gives everything a Project Manager needs while being usable for technical and non-technical team members alike.

If you have the resources go for it.

### Basecamp

[https://basecamp.com/](Basecamp) claim

> For nearly two decades, we’ve continually refined a unique set of tools and methods to fundamentally reduce complexity, and make project management more of a joy and less of a chore. 

I have to say my experience of it is entirely negative. 

There is no cross referencing and even formatting of documents is very limited.

It is a very noisy system that in my experience was dominated by a stream of chatter in which key decisions were lost.

### Documentation folder 

You could just put your documentation in the code repository (and absolutely should do this at least for a README to get people started)

Pros

: available offline

: branches with the code - merge documentation changes with related code changes

: format however you want (just use your favorite static site generator)

Cons

: harder to edit for non-technical team members

: no integration with issue tracking etc


### Github/Gitlab wiki

Both Github and Gitlab offer a wiki for projects which both work in much the same way.

Your wiki exists in Github/Gitlab as a set of web pages with and online editor.

It is backed by a git repo that you can clone for offline use and editing.

The biggest benefit is that they allow all sorts of links/mentions that get suitably cross referenced to issues and trigger suitable notifications.

There is even a tool called [Gollum](https://github.com/gollum/gollum) which allows you to run a local version of the wiki - but it works in much the same way as the website (without the provider specific links) and so I don't think has much benefit - you can make offline edits without it and just use the VSCode markdown preview.

This setup isn't as feature rich at the Atlassian one - but is much cheaper and has an easier learning curve.


### Google Docs / MS Office

All I have ever seen with this is a mess of documents that don't relate to each other and are constantly outdated.

Google docs is especially bad because you can easily miss wether a document is in your dice or just shared with you. 


### Slack / email / etc

Great tools for discussion (though be careful how often you interrupt people) and while sometimes you can find "smoking gun" type information in the archives - it's best to assume that nothing is stored there.

If you have an online discussion and a decision is made - document it separately and reference the source where needed.

## Culture
 
This really is the main thing.

You can have the best tools and it won't help if nobody uses them.

Or you could do it all in Notepad if everyone is heroic.

The best way to build a good culture of project documentation is to have a small number of champions who lead the way on writing good docs and always minuting meetings. Then expect the rest of the team to join in.

Don't send people docs - send them a link to where it is kept or tell them how to find it.

Add documentation time to your estimates and include it in the definition of done.

Documentation dies when it is untrusted - make sure things are updated - if you are uncertain about part of it - add a caveat right there until someone can check it.

Refer back to it - show people how it saves time by avoiding circular conversations and keeps everything moving forwards.

