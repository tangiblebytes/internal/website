#!/bin/bash -ex

source ~/.nvm/nvm.sh

nvm use

cd $(dirname $0);
HUGO_ENV="production" hugo --cleanDestinationDir  --minify
workbox generateSW ./workbox-config.js
firebase deploy --only hosting 