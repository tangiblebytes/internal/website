---
title : "Laravel Vite Wont Serve Build Files"
date : 2025-02-14T16:44:13Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Laravel
 - Vite

---
<div class="clearfix">

![](/laravel-logo-200.png)
{.float-left}

This is a silly one.

I wanted to run Laravel in dev mode but using the compiled js and scss from `npm run build` via vitejs.

This usually works 

but for some reason Laravel was trying to serve js/css from http://localhost:5173/@vite/client 

a service I usually run - but I wanted to test the build stuff.

The solution is really simple but not well documented.

</div>
<!--more-->

To get it working I just had to delete the file `public/hot`

If this file is present the assets are hot loaded - if not the build ones are served.

I guess that `vite run dev` usually cleans this file up on exit - but must have been stopped abruptly at some point.