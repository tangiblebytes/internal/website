---
title : "Golang : Glibc Not Found"
date : 2023-12-13T10:50:15Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Golang

---

I've written some Go code and in development it worked fine but now I need it to run on an old server and I get this error

> ./my-code: /lib64/libc.so.6: version `GLIBC_2.32' not found (required by ./my-code)


<!--more-->

It turns out that some Go binaries depend on Glibc - and the server is too out of date to have a compatible version.

I don't want to upgrade the server.

[musl](https://musl.libc.org/) is a lightweight library that provides most of what glibc does 

In my case it seems to be close enough to work - and it is a lot smaller and easier to embed.


To get it on Linux 

```
sudo apt install musl
sudo apt install musl-dev
```

To build my Go binary with it 

```bash
CC=/usr/bin/x86_64-linux-musl-gcc go build --ldflags '-linkmode external -extldflags "-static"' ./cmd/my-code/
```

Result ! I have a binary that I can scp to my server and "It Just Works"

