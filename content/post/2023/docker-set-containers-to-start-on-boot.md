---
title : "Set Containers to Start on Boot"
date : 2023-06-28T13:09:01+01:00
#images : ["/2016/10/image.jpg"]
description : "How to make sure your Docker containers start up after a reboot."
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Docker

---

If you are running services via docker, how do you make sure they start up after a reboot, after restarting the docker daemon, or just after they 
crash? 

Read this page [Start containers automatically](https://docs.docker.com/config/containers/start-containers-automatically/)

<div class="clearfix">

![](/docker-logo-200.webp)
{.float-left}
</div>
<!--more-->

If you know when you first run the container that you want this 

`docker run -d --restart unless-stopped myimage`

If you want to change an existing conatiner 

`docker update --restart unless-stopped mycontainer`

To check the policy of a running container 

Use docker inspect - and optionally the `jq` utility top parse the JSON

`docker inspect mycontainer | jq -r '.[] | .HostConfig.RestartPolicy  '`
