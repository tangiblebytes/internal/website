---
title: "Windows10 Installer Linux"
date: 2020-10-20T20:54:13+01:00
#images : ["/2016/10/image.jpg"]
description: ""
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - windows
  - linux
---

I repurposed an old workstation and needed to install Windows with only Linux systems to work from.

This turned out to be painful because the system has an old BIOS and while you can in theory install windows from a USB stick - doing so requires a newer system using UEFI

Linux installs fine from a USB stick so this caught me out.

It turns out the Windows installer works fine from an external DVD drive - but first you may have to buy some double layer DVDs because the image doesn't quite fit on a normal one.

Windows installation is easier than it used to be - but still nowhere near as slick as a Linux install.
