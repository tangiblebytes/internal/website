---
title: "Install Reactjs"
date: 2020-05-13T22:35:34+01:00
draft: false
Author: Sean Burlington
tags:
  - JavaScript
  - react
  - nvm
  - planning
---

I've been writing software for long enough to know that it pays to be careful at the start. You don't always know when you'll be supporting a project long term and may need to upgrade versions of your framework and related tools.

These are my notes of starting to use [React](https://reactjs.org/) with an eye to long term support.

<!--more-->

First of all use use [nvm](https://github.com/nvm-sh/nvm) to install node so that you can have different projects on different node versions.

If you need to make a small change on an old project you don't want to be forced to upgrade it just to be able to test.

Start with the [recommended LTS version](https://nodejs.org/en/about/releases/) (unless you know a good reason to do different)

```bash
nvm install --lts=erbium
npx create-react-app my-app
cd my-app
npm start
```

{{< aside >}}

### Error: Cannot find module '../scripts/start'

I got a new workstation and when I copied over my code and tried to run `npm install` I got the above error

I think the cause of this was that my old laptop had node installed from some time before and when I started on React this time I didn't update it.

On my new system I had been careful when I installed to get the best version.

https://github.com/facebook/create-react-app/issues/200

Not pretty - but this fixed it

```bash
rm -rf node_modules
npm install
```

{{< /aside >}}
