---
title : "Digital Ocean Token Expiry"
date : 2025-02-28T16:44:49Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Note To Self
 - Digital Ocean
 - Kubernetes

---

Today while doing a deploy my kubectl command were failing with 


`Error: Failed to fetch credentials for cluster "some-id" Unable to authenticate you`

This was because by doctl token had expired



<!--more-->


I only use doctl as part of the kubernetes authentication 

To fix it I 

 * visited https://cloud.digitalocean.com/account/api/tokens
 * clicked **Generate New Token**
 * filled in a name and set an expiration time 
 * selected **Custom Scopes** and **Kubernetes**
 * clicked **Generate Token**
 * copied the resulting token
 * replaced my old token in `.config/doctl/config.yaml`


 Then my kubectl commands worked again


The DO docs lead me initially to  `doctl auth init` did not work - it didn't prompt me for a new token - maybe this is just for first setup.
