---
title : "Docker, Firewalls, and Minikube"
date : 2024-04-24T15:29:18+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Docker
 - iptables
 - firewalld
 - Minikube
 - Linux

---



<div class="clearfix">

I've had a few problems with docker and firewalls and seem to get a "good enough" solution only to run into trouble again later having forgotten what I've done so far.

This is an attempt to make some notes and at least capture where I am up to.

## TLDR

 * Disable dockers Iptables
 * Add some firewalld rules
 * Watch out if the Docker interface changes

https://dev.to/soerenmetje/how-to-secure-a-docker-host-using-firewalld-2joo


![](/docker-logo-200.webp )
{.float-left}


</div>
<!--more-->


## Docker Exposes Ports on the Public Interface

The starting point for me was realising that when I run a service on Docker and expose that so that I can (for example) test teh website I am developing - that port is exposed on my public network and not just on localhost.

When I'm working on a site it may well be insecure and confidential - I don't want it exposed to whatever is on the same network as me.

## Docker manipulates IPtables

https://docs.docker.com/network/packet-filtering-firewalls/

It looks like things may have improved since I started working on this - and maybe I just need to upgrade Docker 


## Manual Firewall Rules can break Networking for Docker

https://dev.to/soerenmetje/how-to-secure-a-docker-host-using-firewalld-2joo

I followed this guide to get Docker working with firewall rules in place.

The key steps were

### Disable iptables for docker


 /etc/docker/daemon.json
```
{
"iptables": false
}

### Add Masquerading to the zone which leads out to the Internet, typically public 

```
# Masquerading allows for docker ingress and egress (this is the juicy bit)
firewall-cmd --zone=public --add-masquerade --permanent
# Reload firewall to apply permanent rules
firewall-cmd --reload
```

### add docker interface to the trusted zone

in order to enable docker containers accessing host ports 

```
# Assumes docker interface is docker0
firewall-cmd --permanent --zone=trusted --add-interface=docker0
firewall-cmd --reload
systemctl restart docker
```

### enable outgoing internet access for containers

```
# Assumes network interface with your public IP is eth0
firewall-cmd --permanent --zone=public --add-interface=eth0
firewall-cmd --reload
```


## Minikube Seems to Change the Docker Network Interface

Additionally inter-container communication broken when I started minikube - I wasn't sure why at first and thought maybe DNS or docker networking had changed.

Looking at syslog I could see the firewall was blocking connections and they were all from a specific network interface 

This allowed these connections to work 

```
firewall-cmd --zone=trusted --add-interface=br-f4c7e0015bef
```

I don't fully understand what happened here - and it seems good enough for now.

No doubt this will break again but right now I need to work on something else.
