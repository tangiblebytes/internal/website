---
title : "Laravel Force Https Links"
date : 2024-02-13T17:57:31Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Laravel
 - Https
 - Proxy
 - Kubernetes

---


-------------------


Read [Laravel HTTPS Behind a Proxy](./laravel-https-behind-a-proxy) instead

**The post below is wrong**

------------------


This doesn't seem to be well documented - I cant find anything about it in the official docs.

Thanks to [Md Obydullah at shouts.dev](https://shouts.dev/articles/force-laravel-to-use-https-connection)

My Laravel site runs in kubernetes where TLS encryption happens in a proxy layer and I need Laravel to server content with https links.

<!--more-->

Even after I found the first part of this pagination links were still http


Adding this in app/Providers/AppServiceProvider.php fixed it 



```php
/**
* Bootstrap any application services.
*/
public function boot()
{

    if ($this->app->environment('production')) { // includes staging
        URL::forceScheme('https');
            $this->app['request']->server->set('HTTPS','on'); // needed for pagination links
    } else {
        DB::listen(function ($query) {
             Log::info(
                $query->sql,
                $query->bindings
            );
        });
    }
}

```


NB that also adds SQL logging in my dev env (for API development where the debugbar isn't helpful)


-------------------

**The post above is wrong**

Read [Laravel HTTPS Behind a Proxy](./laravel-https-behind-a-proxy) instead

