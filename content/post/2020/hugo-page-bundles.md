---
title: "Hugo Page Bundles"
date: 2020-11-26T14:30:39Z
images: ["/gopher-hero.webp"]
description: ""
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - Hugo
---

Hugo [Page Bundles](https://gohugo.io/content-management/page-bundles/) seem like a great idea, providing a way to organise images and other pages assets.

Here's why I decided not to use them.

![](/gopher-hero.webp)

<!--more-->

This site is a blog - the main aim of the tools is to let me either write and think about the content - or code and think about layout issues etc.

I don't want to context switch in the middle of an article

Page bundles seem to need templating to work - yes I could use shortcodes but I'm not sure what I gain over regular markdown.

I guess I was hoping page bundles would allow me to use templating and gain the option of that power - but to still have the ability to use plain old markdown

The biggest drawback is that every page becomes an index.md which means if I have several pages open in my editor - they all look the same.

I think if I had a more structured site which used images in a more predictable way page bundles would be useful.

But I tend to either use images to add a bit of colour to the page and give people a quick visual indicator of the topic (and even then it's often just a logo).

Or I'm using screenshots to illustrate something and then I probably want to hand code how they are placed.

I can always manually maintain an image structure within static.
