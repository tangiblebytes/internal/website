---
title: "Privacy Policy"
date: 2020-06-04T13:51:35+01:00
draft: false
tags:
  - privacy
  - gdpr
  - cookies
---

## This sites does not use cookies

It is a simple blog and does not need to

## This site does not use analytics

While there could be some benefits to me in seeing which parts of the site are more popular at this time I think the GDPR is essentially a good law which provides important freedoms.

It is right that people should only be tracked with their consent.

That said cookie popups are very annoying and I believe that the value of analytics is less than the costs of annoying people and so have decided not to track users on this site.

## Third party sites may track you

I include links to other sites - these may not have the same privacy rules.

I don't include any third party assets that might track you.
