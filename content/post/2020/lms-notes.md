---
title: "Lms Notes"
date: 2020-10-31T10:44:04Z
#images : ["/2016/10/image.jpg"]
description: ""
#categories : ["category"]
draft: true
author: Sean Burlington
tags:
  - tag1
---

Web interface polls jsonrpc.js every second or so

something like this

```bash
echo '{"id":1,"method":"slim.request","params":["",["serverstatus",0,999]]}' |  POST -H "Cookie: Squeezebox-player=2c%3Af0%3A5d%3A0d%3A6c%3A27; Squeezebox-enableHiDPI=1; Squeezebox-expandPlayerControl=true; Squeezebox-expanded-MY_MUSIC=1; Squeezebox-expanded-RADIO=0; Squeezebox-expanded-PLUGIN_MY_APPS_MODULE_NAME=0; Squeezebox-expanded-FAVORITES=1; Squeezebox-expanded-PLUGINS=0; Squeezebox-playersettings=null; Squeezebox-advancedsettings=null; Squeezebox-expanded-activePlugins=1; Squeezebox-expanded-inactivePlugins=1; Squeezebox-expanded-otherPlugins0=1" http://fruitytunes:9000/jsonrpc.js | python3 -m json.tool
```

Which responds with status of the entire system

```javascript
{
    "id": 1,
    "method": "slim.request",
    "result": {
        "player count": 5,
        "other player count": 0,
        "ip": "192.168.1.118",
        "lastscan": "1603023731",
        "version": "7.9.4",
        "players_loop": [
            {
                "connected": 1,
                "isplayer": 1,
                "modelname": "Squeezebox Radio",
                "canpoweroff": 1,
                "isplaying": 0,
                "seq_no": "51",
                "power": 0,
                "model": "baby",
                "name": "living room",
                "ip": "192.168.1.102:34960",
                "playerindex": "0",
                "displaytype": "none",
                "uuid": "b4f56f6d9b239ea1002a225f5c4c1847",
                "playerid": "00:04:20:2a:94:a2",
                "firmware": "7.7.3-r16676"
            },
            {
                "firmware": "7.7.3-r16676",
                "playerid": "00:04:20:2a:97:fe",
                "uuid": "267d3b2b2e3430b41129ac4489f0de01",
                "playerindex": 1,
                "displaytype": "none",
                "ip": "192.168.1.115:58770",
                "name": "office",
                "power": 1,
                "model": "baby",
                "seq_no": "2",
                "isplaying": 0,
                "canpoweroff": 1,
                "isplayer": 1,
                "modelname": "Squeezebox Radio",
                "connected": 1
            },
            {
                "displaytype": "none",
                "playerindex": 2,
                "uuid": null,
                "playerid": "2c:f0:5d:0d:6c:27",
                "firmware": "v1.8",
                "connected": 1,
                "modelname": "SqueezeLite",
                "isplayer": 1,
                "canpoweroff": 1,
                "isplaying": 0,
                "seq_no": 0,
                "name": "SqueezeLite",
                "power": 1,
                "model": "squeezelite",
                "ip": "192.168.1.121:39876"
            },
            {
                "isplayer": 1,
                "modelname": "Squeezebox Radio",
                "connected": 1,
                "seq_no": "274",
                "canpoweroff": 1,
                "isplaying": 0,
                "ip": "192.168.1.104:45065",
                "model": "baby",
                "power": 0,
                "name": "Kitchen",
                "playerindex": 3,
                "displaytype": "none",
                "uuid": "69b67f03d042965ff41176261c9b9c8f",
                "firmware": "7.7.3-r16676",
                "playerid": "00:04:20:2a:97:fd"
            },
            {
                "playerid": "00:04:20:2a:98:29",
                "firmware": "7.7.3-r16676",
                "uuid": "dbe438ba3d32e009d03cbec5b0f41845",
                "playerindex": 4,
                "displaytype": "none",
                "name": "bedroom battery",
                "power": 0,
                "model": "baby",
                "ip": "192.168.1.101:48219",
                "canpoweroff": 1,
                "isplaying": 0,
                "seq_no": "421",
                "connected": 1,
                "isplayer": 1,
                "modelname": "Squeezebox Radio"
            }
        ],
        "info total songs": 4004,
        "info total genres": 63,
        "info total albums": 369,
        "httpport": "9000",
        "sn player count": 0,
        "mac": "b8:27:eb:f2:9f:a8",
        "uuid": "054e03ec-1b06-40ec-93b9-2ccd4ab58d7a",
        "info total artists": 767,
        "info total duration": 1336239.88
    },
    "params": [
        "",
        [
            "serverstatus",
            "0",
            "999"
        ]
    ]
}

```

telnet session

```bash
sean@rex:~$ telnet fruitytunes 9090
Trying 192.168.1.118...
Connected to fruitytunes.
Escape character is '^]'.
version ?
version 7.9.4
can info total genres ?
can info total genres 0
genres
genres   count%3A63
genres search
genres search  id%3A102 genre%3A255) id%3A126 genre%3A80 id%3A97 genre%3A80's id%3A80 genre%3AAcoustic id%3A72 genre%3AAlternative id%3A89 genre%3AAlternative%20%26%20Punk id%3A90 genre%3AAlternative%20Rock id%3A95 genre%3AAlternRock id%3A125 genre%3AAmbient id%3A98 genre%3AAudiobook id%3A110 genre%3ABallad id%3A70 genre%3ABluegrass id%3A101 genre%3ABlues id%3A119 genre%3ABossa%20Nova id%3A114 genre%3AChill%20Out id%3A86 genre%3AClassic%20Rock id%3A75 genre%3AClassical id%3A77 genre%3AComedy id%3A91 genre%3ACountry id%3A122 genre%3ACovers id%3A109 genre%3ADance id%3A65 genre%3ADance%20%26%20DJ id%3A107 genre%3AEasy%20Listening id%3A83 genre%3AElectronic id%3A113 genre%3AElectronica%2FDance id%3A74 genre%3AFolk id%3A76 genre%3AFolk%2FRock id%3A79 genre%3AFunk id%3A111 genre%3AGeneral%20Alternative id%3A118 genre%3AGeneral%20House id%3A116 genre%3AGeneral%20Reggae id%3A106 genre%3AGeneral%20Rock id%3A115 genre%3AGenre id%3A120 genre%3AHouse id%3A88 genre%3AIndie id%3A92 genre%3AInternational id%3A117 genre%3AJazz id%3A121 genre%3ALatin id%3A96 genre%3AMisc id%3A87 genre%3ANew%20Age id%3A105 genre%3ANew%20Wave id%3A64 genre%3ANo%20Genre id%3A108 genre%3AOldies id%3A100 genre%3AOpera%20%26%20Vocal id%3A81 genre%3AOther id%3A66 genre%3APop id%3A99 genre%3APop-Folk id%3A124 genre%3APost-rock id%3A93 genre%3AR%26B id%3A73 genre%3AReggae id%3A69 genre%3ARock id%3A104 genre%3ARock%2FPop id%3A85 genre%3ARock%20%26%20Roll id%3A112 genre%3ARockPop id%3A103 genre%3ASoft%20Rock id%3A71 genre%3ASoul id%3A78 genre%3ASoul%20and%20R%26B id%3A67 genre%3ASoundtrack id%3A68 genre%3ASoundtracks id%3A84 genre%3ASwing id%3A94 genre%3AWorld id%3A82 genre%3AWorld%20Music id%3A123 genre%3AX%20Rated%20Techno count%3A63
^C

^]
telnet> Connection closed.
sean@rex:~$ echo "genres search" | telnet fruitytunes 9090
Trying 192.168.1.118...
Connected to fruitytunes.
Escape character is '^]'.
Connection closed by foreign host.
sean@rex:~$ telnet fruitytunes 9090
Trying 192.168.1.118...
Connected to fruitytunes.
Escape character is '^]'.
genres search
genres search  id%3A102 genre%3A255) id%3A126 genre%3A80 id%3A97 genre%3A80's id%3A80 genre%3AAcoustic id%3A72 genre%3AAlternative id%3A89 genre%3AAlternative%20%26%20Punk id%3A90 genre%3AAlternative%20Rock id%3A95 genre%3AAlternRock id%3A125 genre%3AAmbient id%3A98 genre%3AAudiobook id%3A110 genre%3ABallad id%3A70 genre%3ABluegrass id%3A101 genre%3ABlues id%3A119 genre%3ABossa%20Nova id%3A114 genre%3AChill%20Out id%3A86 genre%3AClassic%20Rock id%3A75 genre%3AClassical id%3A77 genre%3AComedy id%3A91 genre%3ACountry id%3A122 genre%3ACovers id%3A109 genre%3ADance id%3A65 genre%3ADance%20%26%20DJ id%3A107 genre%3AEasy%20Listening id%3A83 genre%3AElectronic id%3A113 genre%3AElectronica%2FDance id%3A74 genre%3AFolk id%3A76 genre%3AFolk%2FRock id%3A79 genre%3AFunk id%3A111 genre%3AGeneral%20Alternative id%3A118 genre%3AGeneral%20House id%3A116 genre%3AGeneral%20Reggae id%3A106 genre%3AGeneral%20Rock id%3A115 genre%3AGenre id%3A120 genre%3AHouse id%3A88 genre%3AIndie id%3A92 genre%3AInternational id%3A117 genre%3AJazz id%3A121 genre%3ALatin id%3A96 genre%3AMisc id%3A87 genre%3ANew%20Age id%3A105 genre%3ANew%20Wave id%3A64 genre%3ANo%20Genre id%3A108 genre%3AOldies id%3A100 genre%3AOpera%20%26%20Vocal id%3A81 genre%3AOther id%3A66 genre%3APop id%3A99 genre%3APop-Folk id%3A124 genre%3APost-rock id%3A93 genre%3AR%26B id%3A73 genre%3AReggae id%3A69 genre%3ARock id%3A104 genre%3ARock%2FPop id%3A85 genre%3ARock%20%26%20Roll id%3A112 genre%3ARockPop id%3A103 genre%3ASoft%20Rock id%3A71 genre%3ASoul id%3A78 genre%3ASoul%20and%20R%26B id%3A67 genre%3ASoundtrack id%3A68 genre%3ASoundtracks id%3A84 genre%3ASwing id%3A94 genre%3AWorld id%3A82 genre%3AWorld%20Music id%3A123 genre%3AX%20Rated%20Techno count%3A63
favorites
favorites
favorites play
favorites play
favorites items
favorites items   count%3A45
favorites items 0 45
favorites items 0 45 title%3AFavorites id%3A0 name%3AA%20Hundred%20Miles%20Or%20More%3A%20A%20Collection type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A1 name%3AA%20Rush%20Of%20Blood%20To%20The%20Head type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A2 name%3ABBC%20Radio%204 type%3Aaudio isaudio%3A1 hasitems%3A0 id%3A3 name%3ABBC%20Radio%204 type%3Aaudio isaudio%3A1 hasitems%3A0 id%3A4 name%3AThe%20Platinum%20Collection%20-%20Greatest%20Hits%20I type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A5 name%3A569.8 type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A6 name%3AAll%20the%20Little%20Lights type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A7 name%3ADeclaration%20of%20Dependence type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A8 name%3AThe%20Dream%20of%20the%20Blue%20Turtles type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A9 name%3AGarden%20State type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A10 name%3AGraceland type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A11 name%3AHarry%20Potter%20And%20The%20Chamber%20Of%20Secrets type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A12 name%3AHarry%20Potter%20And%20The%20Deathly%20Hallows type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A13 name%3AHarry%20Potter%20And%20The%20Half-Blood%20Prince type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A14 name%3AHarry%20Potter%20And%20The%20Order%20Of%20The%20Phoenix type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A15 name%3AHarry%20Potter%20And%20The%20Philosopher's%20Stone type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A16 name%3AHarry%20Potter%20And%20The%20Prisoner%20Of%20Azkaban type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A17 name%3AHarry%20Potter%20And%20The%20Goblet%20Of%20Fire type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A18 name%3Aicomputor type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A19 name%3AJuno type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A20 name%3AKill%20Bill%3A%20Vol.%201 type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A21 name%3AKill%20Bill%3A%20Vol.%202 type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A22 name%3ALegend type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A23 name%3AMissing...%20Presumed%20Having%20a%20Good%20Time type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A24 name%3AMoon%20Safari type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A25 name%3AO%20Brother%2C%20Where%20Art%20Thou%3F type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A26 name%3APaper%20Airplane type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A27 name%3AOur%20Time%20In%20Eden type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A28 name%3APIGALLE type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A29 name%3APort%20Isaac's%20Fisherman's%20Friends type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A30 name%3AQuiet%20Is%20The%20New%20Loud type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A31 name%3AScrubs type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A32 name%3ASeal type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A33 name%3ASing%20When%20You're%20Winning type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A34 name%3ASupernature type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A35 name%3ASwing%20When%20You're%20Winning type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A36 name%3ATalkie%20Walkie type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A37 name%3ATAYLOR%20MADE%20-%20GREATEST%20HITS type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A38 name%3ATimepieces%20-%20The%20Best%20Of%20Eric type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A39 name%3ATrailer%20Park type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A40 name%3AVersus type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A41 name%3AVeneer type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A42 name%3AThe%20Very%20Best%20Of%20Fairground%20Attraction type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A43 name%3AWalk%20The%20Line type%3Aplaylist isaudio%3A1 hasitems%3A1 id%3A44 name%3AWelcome%20to%20the%20Beautiful%20South type%3Aplaylist isaudio%3A1 hasitems%3A1 count%3A45
```
