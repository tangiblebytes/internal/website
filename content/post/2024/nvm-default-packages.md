---
title : "Nvm Default Packages"
date : 2024-04-09T14:59:21+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - JavaScript
 - NVM
 - Yarn
 - NoteToSelf

---

This is one of those things I wont need again for a while and will forget.

When using nvm to get a new version of nodejs you can find you no longer have packages - including yarn 

<!--more-->

https://github.com/nvm-sh/nvm#default-global-packages-from-file-while-installing

> If you have a list of default packages you want installed every time you install a new version, just add the package names, one per line, to the file $NVM_DIR/default-packages. You can add anything npm would accept as a package argument on the command line.