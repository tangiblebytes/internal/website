---
title : "Laravel Validation using DatabaseRule"
date : 2024-07-22T16:20:57+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - laravel
 - database

---
<div class="clearfix">

![](/laravel-logo-200.png)
{.float-left}

The Laravel database validation rules are more powerful and flexible than I realised at first.

Looking at the [Available Validation Rules](https://laravel.com/docs/11.x/validation#available-validation-rules) we can see that [Exists](https://laravel.com/docs/11.x/validation#rule-exists) and [Unique](https://laravel.com/docs/11.x/validation#rule-unique) both have the suffix (Database)

This is because the both implement the [DatabaseRule](https://laravel.com/api/11.x/Illuminate/Validation/Rules/DatabaseRule.html) trait which offers more power than might be expected.


</div>
<!--more-->

While the Laravel documentation does hint at this functionality with a couple of examples it is well worth reading the [Laravel API docs](https://laravel.com/api/11.x/Illuminate/Validation/Rules/DatabaseRule.html) for this trait and even reviewing the source code.

These database rules are constructed with arguments providing the table and column names.

Once constructed we can chain on a number of additional constraints


where : Set a "where" constraint on the query

whereNot : Set a "where not" constraint on the query

whereNull : Set a "where null" constraint on the query

whereNotNull : Set a "where not null" constraint on the query

whereIn : Set a "where in" constraint on the query

whereNotIn : Set a "where not in" constraint on the query

withoutTrashed : Ignore soft deleted models during the existence check

onlyTrashed : Only include soft deleted models during the existence check


The best place to understand how to call these is the [source code](https://github.com/laravel/framework/blob/11.x/src/Illuminate/Validation/Rules/DatabaseRule.php)

In my case I have a multisite project where each article has a slug that has to be unique - but only for that site.

Each article has a site_id field which specifies the site the article is for.

My Validation rules 

```php
$request->validate([
    'slug' => ['required', 'string', 'max:255',
        Rule::unique('articles')
        ->where('site_id', $article->site_id)
        ->ignore($article->id),
    ],
    'title' => 'required|string|max:255',
    'synopsis' => 'required|string|max:1000',
    'published_at' => 'nullable|date',
]);
```


The *where* parameters can take a number of forms but to me the most readable option is just column name and column value. 

The *ingore* section seems to me to be something that should be the default - it just says allow the new value to be the same as the old value for the same article.