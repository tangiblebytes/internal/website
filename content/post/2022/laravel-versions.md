---
title : "Laravel Versions"
date : 2022-11-16T10:44:02Z
images : ["/laravel-logo-200.png"]
description : "Laravel - what version do I have, what is the difference between Laravel-framework and Laravel skeleton? What happens when I run composer update?"
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Laravel
 - Composer
 - Versioning

---

<div class="clearfix">

![](/laravel-logo-200.png)
{.float-left}

How to find out what version of Laravel you have.

The difference between a laravel-framework update and a laravel skeleton update.

What is Laravel skeleton anyway ?

How to control when you get updates.

</div>

<!--more-->

## Get Current Version

I can run artisan to get the current version

```bash
php artisan --version 
Laravel Framework 9.40.1
```

and I can look in this file

`vendor/laravel/framework/src/Illuminate/Foundation/Application.php`

```php
    /**
     * The Laravel framework version.
     *
     * @var string
     */
    const VERSION = '9.40.1';
```

But note that this is in vendor and isn't normally committed to source Control

## The Start of a Laravel Project

 The initial [setup of a Laravel project](https://laravel.com/docs/9.x/installation#your-first-laravel-project)

looks like 

```bash
composer create-project laravel/laravel example-app
```

Looking at the docs for [composer create-project](https://getcomposer.org/doc/03-cli.md#create-project)

We see that 

> This is the equivalent of doing a git clone/svn checkout followed by a composer install of the vendors.

That's not quite true - it's a git clone but without all the .git info and history.

Also the Laravel Composer setup triggers a few post install actions

```php
    "scripts": {
        "post-autoload-dump": [
            "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
            "@php artisan package:discover --ansi"
        ],
        "post-update-cmd": [
            "@php artisan vendor:publish --tag=laravel-assets --ansi --force"
        ],
        "post-root-package-install": [
            "@php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ],
        "post-create-project-cmd": [
            "@php artisan key:generate --ansi"
        ]
```

So what you get at this point is the project [laravel/laravel](https://github.com/laravel/laravel)

By default you get the latest version of _laravel/laravel_ aka _Laravel Skeleton_.

This then installs dependencies including _laravel/framework_ 

## Skeleton vs Framework 

The Skeleton is the start of the code you will write app config routes tests - all that stuff.

It's quite "bare bones" but is a functional site for you to build up.

The dependency laravel/framework is where all the core Laravel functionality is.

That can be upgraded with composer - your skelton never gets upgraded by composer.

The Laravel team sometimes release new features for the skeleton - you don't get these by default but you can always modify your code to include the same functionality.

As an example [laravel/laravel version 9.3.0](https://github.com/laravel/laravel/releases/tag/v9.3.0) added [Laravel Pint](https://laravel.com/docs/9.x/pint#introduction) 

> Pint is automatically installed with all new Laravel applications so you may start using it immediately. 

You won't get this via a composer update - you can add it to your own Laravel project you just have to look at the laravel/laravel project and see which changes to copy over.



## Versioning 

[Laravel versioning](https://laravel.com/docs/9.x/releases) is based on the breaking changes - the theory goes that if you're on version 9 you can upgrade to any 9.x without problem. SO the main this is what major version number you are on and the  `php artisan --version` tells us more than enough.

If you want to dig deeper take a look at your composer.json


you should see a line like 

```json
        "laravel/framework": "^9.2",
```

[Composer versioning](https://getcomposer.org/doc/articles/versions.md#caret-version-range-) defines this as caret versioning

> For example ^1.2.3 is equivalent to >=1.2.3 <2.0.0 as none of the releases until 2.0 should break backwards compatibility. For pre-1.0 versions it also acts with safety in mind and treats ^0.3 as >=0.3.0 <0.4.0 and ^0.0.3 as >=0.0.3 <0.0.4.

So in my case this means at least version 9.2 (and tells me that likely laravel was at 9.2 when the project started) but it can install any version of the framework before 10.0

Each time `composer update` is run it will update the framework in vendors

## How to Keep Control

Commit `composer.lock` to the repo and make sure `composer update` is only run at planned intervals.

The rest of the time `composer install` should be sufficient to install packages at the version specified - everyone has the same versions - and there are no surprise updates in production builds.

If you do upgrade any packages and want to reset - do a `git checkout` to get back the old composer.lock delete everything in vendor, and do a fresh `composer install`

