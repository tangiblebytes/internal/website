---
title : "Docker and Dependency Management"
date : 2023-10-03T09:26:39+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : true
author : Sean Burlington
tags :
 - Docker
 - Dependency Management
 - Node
 - Gitlab

---
<div class="clearfix">

docker tags
https://www.mend.io/free-developer-tools/blog/overcoming-dockers-mutable-image-tags/
https://gds-way.cloudapps.digital/manuals/programming-languages/docker.html#using-tags-and-digests-in-from-instructions

dependabot
https://dev.to/oracle2025/how-to-keep-a-dockerfile-updated-with-dependabot-1mdn
https://github.com/dependabot/dependabot-core/tree/main#how-to-run-dependabot

rennovate
https://github.com/renovatebot/renovate
https://docs.renovatebot.com/examples/self-hosting/#gitlab-cicd-pipeline
https://github.com/renovatebot/docker-renovate/blob/main/docs/gitlab.md
https://dev.to/nfrankel/renovate-a-dependabot-alternative-2h8d


dependency-checker 
https://owasp.org/www-project-dependency-check/
has SOnarQube plugin
limted Golang support


https://owasp.org/www-project-dep-scan/


Gitlab container scanning 
https://docs.gitlab.com/ee/user/application_security/container_scanning/


Gitlab dependency scanning (Ultimate)
https://docs.gitlab.com/ee/user/application_security/dependency_scanning



</div>
<!--more-->
