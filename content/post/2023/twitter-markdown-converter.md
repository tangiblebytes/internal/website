---
title : "Twitter Markdown Converter"
date : 2023-01-13T13:12:00Z
#images : ["/2016/10/image.jpg"]
description : "This is a small program I wrote to convert a twitter archive (which is a big ball of Javascript) to a Markdown file."
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Twitter
 - Markdown
 - Golang

---
<div class="clearfix">

![](/Go-Logo-200.png)
{.float-left}

This is a small program I wrote to convert a twitter archive (which is a big ball of Javascript) to a Markdown file.


</div>

<!--more-->


It's not a complex or highly polished program - but it does that I needed it to do - and runs fast.

[tangiblebytes/twitter-to-markdown](https://gitlab.com/tangiblebytes/twitter-to-markdown)


