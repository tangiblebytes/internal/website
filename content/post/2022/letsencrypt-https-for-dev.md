---
title : "Let's Encrypt Https for Dev"
date : 2022-12-20T08:26:48Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - TLS

---
<div class="clearfix">

![](/letsencrypt-logo-lockonly-200.png)
{.float-left}

It's amazing being able to get free TLS certificates instantly from Let's Encrypt.

But they don't provide [certificates for localhost](https://letsencrypt.org/docs/certificates-for-localhost/)

You can generate, and trust, your own certificates and I've written up [how to do that](/2020/how-to-create-valid-local-ssl-tls-https/) but honestly it's not a trivial process.

Here is how to use Let's Encrypt for local dev.

</div>
<!--more-->

All Let's Encrypt needs is a valid domain name and proof that you own it.

So, assuming that you own a domain:

Create a name for your local server

:   something like dev.tangiblebytes.co.uk

Find the IP address

:   I'm using docker compose so I'll set a fixed IP in dockers network and use this 
:   So I have a public DNS record pointing to a private IP (which only works on my network)

Create the DNS record

: Using your providers control panel

Get certbot

: https://certbot.eff.org/

Generate the Certificate

:   This defaults to an automated process that requires a public webserver to work, We can instead use a manual process. 
:   `sudo certbot certonly --manual -d dev.tangiblebytes.co.uk --preferred-challenges dns`
:   You will be asked to add a DNS TXT record - do this via your DNS control panel

Copy the certificate to your webserver

:   ssl_certificate fullchain.pem
:   ssl_certificate_key privkey.pem

Keep the certificates secure 

: Do not share them in a git repo 
: I know this is only dev but this is a real certificate - get in the habit of keeping it secure.

## Summary

This is a fairly quick and easy way to get a valid TLS certificate - it gains you experience of using certbot and Let's Encrypt.

It does require a domain name.

The certificate will expire in a few months - and needs to be renewed.

It makes dev a lot more like production as far as the browser is concerned and that is a very good thing for testing.


