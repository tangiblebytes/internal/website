---
title: "About"
date: 2020-04-29T11:58:00+01:00
draft: false
---

[Sean Burlington](https://www.linkedin.com/in/seanburlington/)

Over 20 years professional experience of website and infrastructure development with clients in a wide variety of settings including government, business, and charity.

Experience working with major brands such as Intel, The Home Office, and The Ramblers - as well as many small organisations.

Broad and deep skillset as a full stack developer, and experienced systems administrator (DevOps).

Capable of working on most parts of a system from database to API code to JavaScript and CSS - I am not a designer but have a strong awareness of how design, brand identity, and business goals impact on a project - while UX and accessibility are cornerstones of delivery.

Plenty of client facing and team management experience.

Values include candour and integrity - with a passion for open source code and a willingness to admit what I don't know.

Ensuring that the code developed delivers value, that it works in production, that the path from laptop to server is as smooth and efficient as possible.

To achieve these goals I follow the principles of DevOps

Experience of projects of all scales - from solo to teams of over a hundred.

Long term (several projects that were live for over 5 years) or short term builds that last only a few weeks.

Cross boundary problem solving: those things that people describe as "random" "weird bugs" that can be caused by caching, race conditions, mismatched interfaces - or may even be a UX problem. To resolve these problems often requires addressing the organisational relationships, attending planning meetings and aligning client expectations with challenging but honest goals.

Building and managing automated systems using whichever tools fit the bill - whether this is for a build pipeline, application testing, config management, container management, reporting etc. These systems form part of working practices that make it much easier for teams to do the right things - but ultimately I work on the principle that it is teams not technology that define the success or failure of any project.

[![This website is hosted Green - checked by thegreenwebfoundation.org](/tangiblebytescouk-greenhost.png)](https://www.thegreenwebfoundation.org/green-web-check/?url=tangiblebytes.co.uk)
