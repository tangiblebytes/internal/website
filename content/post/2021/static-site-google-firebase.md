---
title: "Firebase Hosting"
date: 2021-02-26T10:30:21+01:00
images: ["/firebase-logo-built_white.png"]
description: "Google's Firebase Hosting is perfect for many types of site - especially developer managed projects where low budget, minimal maintenance, and high performance matter."
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - Firebase
  - GCP
  - Hugo
  - Cheap
  - Hosting
---

Website developers (like myself) often end up with a few websites we support - whether these are promoting a business, running a blog, for a side project, or a site for a friend or hobby.

Since these are often low to no budget and can't be prioritised for work at busy times they need to be

- cheap
- low maintenance
- easy to set up
- blazingly fast

<!--more-->

I have found [Static Site Generators](https://jamstack.org/generators/) (I like [Hugo](https://gohugo.io/)) to be excellent for these sites as they are fast to update when I want to, highly configurable, and yet require zero maintenance when not being updated.

Static sites combined with Firebase hosting (and it's use of CDNs) make for a very fast website.

Google's [Firebase](https://firebase.google.com/) doesn't have a high profile but I found it to be a near perfect platform for this type of site.

![](/firebase-logo-built_white.svg)

## What's so Good About it?

While it is a [paid platform](https://firebase.google.com/pricing) - fees only kick in once you reach a threshold

Hosting is free up to 360Mb per day

You can use a custom domain if you have one at no extra charge - you even get an ssl certificate.

As well as static hosting you can run cloud functions and use their [Firestore](/tags/firestore) database.

## Limitations

This isn't a virtual server, you can't run WordPress or Drupal here

It's not all point and click - you do need some command line tools to run it.

## Advantages Over AWS

Amazon Web Services are hugely popular and I think many developers (myself included) are tempted to just use AWS because that's what "everyone" does and it seems like experience there would be useful.

I did try to setup a similar service there using S3, Cloudfront, and Lambdas

But that's the thing - I had to use several services and glue them together. I needed Lambdas just to set headers

Even adding [directory indexing](https://aws.amazon.com/blogs/compute/implementing-default-directory-indexes-in-amazon-s3-backed-amazon-cloudfront-origins-using-lambdaedge/) was difficult and I ended up using [ugly urls](https://gohugo.io/content-management/urls/#ugly-urls) instead.

With Firebase everything pretty much worked out of the box - and things I wanted to fine tune were easy to setup with the config file [firebase.json](https://firebase.google.com/docs/hosting/full-config)

## How to Set it up

I'm going to assume you have a static site already - or you can just create a hello world index page to test with.

Assuming you have a google account

Sign into the [firebase console](https://console.firebase.google.com/)

Really just click on create a project and follow the instructions there.

It's so easy it isn't worth writing up - except I'll do it anyway because otherwise you'll imagine it is more complex than it is.

- Create a Project
  ![](/create-a-project.webp)

- Give it a name
  ![](/give-it-a-name.webp)

  It has to be a name unique among all global Firebase instances (not just among yours) If you use a name someone else has used Google will append a random string to it  
  The name will be used as the project name in your Firebase dashboard and also as default hostname using the domains web.app and firebaseapp.com  
  e.g. some-unique-name.web.app

  You can add a custom domain later

- Choose whether to enable Google Analytics
  ![](/choose-if-you-want-google-analytics.webp)  
  I have never used in on a project because I prefer not to have a cookie banner but it's there if you want it.

- Wait a few moments till the project is ready
  ![](/wait-till-its-ready.webp)

- Follow the steps (Google will walk you through this)

  navigate to or create a root directory for your web app

  ```bash
  npm install -g firebase-tools
  firebase login
  firebase init
  firebase deploy
  ```

That's it - your site will be live.

Whatever html/css/js/images etc you had in the directory you defined as your web root will now be on a live server

To edit this site change the contents of your folder and run `firebase deploy` again.

The nice things about this are that

Updates are atomic : the files are synced and then Firebase flips over to the new site when ready.

Rollback is trivial : a release history is retained and you can roll back to any earlier release at the click of a button (how many old releases you store is configurable)

## Going Further

I've found Firebase to be well documented and new features are being actively developed.

As well as using Firebase for static hosting I have used it with its Cloud Functions and Firestore database - those are great too -but there is more to learn.

The really amazing thing about Firebase hosting is just how easy it is.

and for sites like this I haven't paid a penny.

Google seem to have marketed Firebase as a mobile app solution - and it looks great for that - but it also works perfectly for a Static site with Cloud Functions and a NoSQL database and minimal setup.
