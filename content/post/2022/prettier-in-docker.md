---
title: "Prettier in Docker"
date: 2022-11-12T15:32:33Z
#images : ["/2016/10/image.jpg"]
description: "How to run prettier.io in Docker Compose without having to setup node locally."
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - Coding
  - Format
  - Prettier
---
<div class="clearfix">

![](/docker-logo-200.webp)
{.float-left}


I'm working on a PHP project and code review brought up some formatting issues in my CSS.

I wanted to added automated formatting of these files accessible to everyone in the team without them having to install node as this isn't a JavaScript project.

We already use Docker so that seemed an obvious choice.
</div>
<!--more-->

At first I thought I should install node on our webserver container - but that just shows I haven't fully made the mental leap to containers - they are not virtual machines and we should have a separate container for this task.

Also this container only needs to run when we want to run a command on it - it isn't a sever. Once the formatting is finished it can exit and we will start a new one next time.


We already had a Dockerfile - this is just part of it showing the PHP service and the new node service (there is also a webserver and a database)

Lines 18-29 below relate to running JavaScript

```yaml {linenos=true,hl_lines=["18-29"]}
version: "3"
services:
  #PHP Service
  app:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: app
    restart: unless-stopped
    tty: true
    working_dir: /var/www
    volumes:
      - ./:/var/www
      - ./php.ini:/usr/local/etc/php/conf.d/local.ini
    networks:
      - app-network

  node:
    # don't start this by default (it only runs when
    profiles: ["nodejs"]  running a command)
    image: node:lts-alpine3.16
    user: node
    container_name: node
    tty: true
    working_dir: /var/www
    volumes:
      - ./:/var/www
    networks:
      - app-network

```

I'm using a standard [node alpine image](https://github.com/nodejs/docker-node/tree/main/18/alpine3.16) - this has node 18 on a minimal linux base.

Node is installed for the user "node" uid 1000 and gid 1000 

The other containers use the same uid/gid with a different name (and it's the IDs that matter here).

I mount my files at the same place on both systems so any paths will match.

I wanted `docker-compose up` to only start my servers - and not this node container which is only needed when a command is run.

It seems the way to do this is [profiles](https://docs.docker.com/compose/profiles/)


> If unassigned, the service is always started but if assigned, it is only started if the profile is activated.
 
so by default my unassigned services will come up but not my assigned one which only runs when I tell it to.

Now I can run node commands using [docker-compose run](https://docs.docker.com/engine/reference/commandline/compose_run/) 

This creates a container, runs the command on it and removes the container.

It's still quick because docker caches the images - but it doesn't take up much resource.

`docker-compose run --rm node npm -v`

Checks it is running correctly

Now I can [install prettier](https://prettier.io/docs/en/install.html).

`docker-compose run --rm node npm install --save-dev --save-exact prettier`


Note that npm install runs on the container but since it installs files in a shared folder my project package.json and related files will be changed.

This together with the Docker changes mean that prettier is available to the whole team with no code install - just git pull and docker-compose (re)build.

Create an empty `.prettier.json` file

```json
{}
```

`.prettierignore`

Mine looks like this

```txt
node_modules
storage/
vendor
.phpunit.result.cache
phpunit_reports/
.scannerwork/
.idea
.vscode
```


Now I can run and check it works 

`docker-compose run --rm node npx prettier -c .`

and when happy it works fix all the files

`docker-compose run --rm node npx prettier -w .`






I found these sites informative 

- [Prettier.io](https://prettier.io/)
- [Using Prettier in PHP](https://madewithlove.com/blog/software-engineering/using-prettier-in-php/)
- [Prettier options](https://prettier.io/docs/en/options.html)