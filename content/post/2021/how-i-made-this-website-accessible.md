---
title: "How I Made This Website Accessible"
date: 2021-02-16T14:00:22Z
#images : ["/2016/10/image.jpg"]
description: ""
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - Accessibility
  - This website
---

One of the things I love about the web is the flexibility it brings.

How I see this site may not be how you see it - and maybe you don't see it at all but are using a screen reader.

<!--more-->

Making a site work well on web and mobile is part of it.

The next steps are making sure it works for people with poor vision, no vision, poor motor control, and cognitive issues.

My site is mostly text, I don't have any of the bigger challenges I would have on a rich JavaScript driven web app - but I still found issues.

My first stop was to test the site at https://wave.webaim.org/report#/https://tangiblebytes.co.uk/

## Contrast Issues

These were the worst problem I had - too much of the text was either grey on white, or pale blue on white.

So I made things like links a darker blue, changed the top navbar to a darker blue and made the links there whiter.

I found the code sections had low contrast syntax highlighting - fortunately I found [some high contrast stylesheets](https://github.com/mpchadwick/)

## Link Spacing

I have lists of links - adding more padding to separate these makes it easier to see where a line has wrapped and easier to click in the right place.

This separation is important both for mobiles where tapping may be inaccurate and for people who may struggle to accurately control a mouse.

## Link Tabbing

You can use the keyboard to navigate most sites - use tab to go to the next link and enter to navigate to it.

Some sites have hidden links or things you can click on that are not links so this can be a challenge.

I didn't think I would have any issues here but I was wrong.

Nothing major but I had to remove a button in the pagination (previous or next) that was only visually disabled when not needed.

## Results

This isn't the result of a formal audit and I haven't described every addressed issue here but I think the result is good.

By making some changes that would have been a barrier to some people I think it has ended up being easier to use for everyone.
