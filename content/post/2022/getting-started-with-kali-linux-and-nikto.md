---
title : "Getting Started With Kali Linux and Nikto"
date : 2022-12-28T13:29:31Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Security
 - Kali
 - Linux
 - Nikto

---


As web developers we are used to people mostly using teh websites we build in teh way intended.

It can be hard to get into the mindset not just of what can go wrong but how what we have built can be subverted.

There are a lot of tools available to those who want to attack our websites - and I think it is worth web developers having some familiarity with these.

The tools themselves are legal - but should only be used where you have permission.


<!--more-->

## Kali Linux

- https://www.kali.org/

One of the main toolkits used (because it is easy to get started with) is Kali Linux

It is a full Linux distro based on Ubuntu, with a patched kernel that facilitates network attacks by doing things that networking shouldn't do.

Most of the tools though are commandline and for web testing we don't need the low level network attacks so the docker version of Kali makes for an easier starting point.

Kali has many tools available (either pre-installed or via apt) https://www.kali.org/tools/

## Setup a container with Kali

```bash
docker run --tty --interactive --name=kali kalilinux/kali-rolling
```

This will download the image, start a container (named kali) and put you in the shell as root.

The image doesn't have much installed to start with so we'll need to run apt.

```bash
apt-get update && apt-get upgrade
apt-get install nikto
```

## Nikto

Above I've installed [Nikto](https://www.kali.org/tools/nikto/) I'm not sure it's the best tool - but it is very easy to get started with and gives some interesting results.

Alternatives are gobuster and dirbuster 

I wanted to test another container so first (from my host) I needed to add my kali container to the network my container is on.


```bash
docker network connect mynetwork kali 
``

If you exited your docker container you can get back in by running

```bash
docker start  kali 
docker attach kali
```

Now run the scan

```bash
nikto -h https://myhost.dev.tangiblebytes.co.uk/
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          192.168.55.12
+ Target Hostname:    myhost.dev.tangiblebytes.co.uk
+ Target Port:        443
---------------------------------------------------------------------------
+ SSL Info:        Subject:  /CN=grassroots.dev.tangiblebytes.co.uk
                   Altnames: myhost.dev.tangiblebytes.co.uk
                   Ciphers:  ECDHE-RSA-AES256-GCM-SHA384
                   Issuer:   /C=US/O=Let's Encrypt/CN=R3
+ Start Time:         2022-12-28 10:26:40 (GMT0)
---------------------------------------------------------------------------
+ Server: nginx/1.23.3
+ Retrieved x-powered-by header: PHP/8.2.0
+ The site uses SSL and the Strict-Transport-Security HTTP header is not defined.
+ The site uses SSL and Expect-CT header is not present.
+ Cookie XSRF-TOKEN created without the secure flag
+ Cookie XSRF-TOKEN created without the httponly flag
+ Cookie laravel_session created without the secure flag
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ Allowed HTTP Methods: GET, HEAD 
+ /info.php: Output from the phpinfo() function was found.
+ OSVDB-3233: /info.php: PHP is installed, and a test script which runs phpinfo() was found. This gives a lot of system information.
+ OSVDB-5292: /info.php?file=http://cirt.net/rfiinc.txt?: RFI from RSnake's list (http://ha.ckers.org/weird/rfi-locations.dat) or from http://osvdb.org/
+ Retrieved access-control-allow-origin header: *
+ 8019 requests: 0 error(s) and 11 item(s) reported on remote host
+ End Time:           2022-12-28 10:37:08 (GMT0) (628 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

Note that this took about 10 minutes and made 8 thousand requests 

What it does is simply to try loads of possible urls that might have a weakness.

It's looking for things like database dumps, admin logins, debug pages - loads of common misconfigurations.

Look at the logs on your server while the test is running - it's mostly brute force - but also the hackers know about all of the stupid mistakes people make.

My test site is a brand new laravel site - I haven't really started development on it yet and it is firewalled on my local system.


## What can I learn from my scan?

I should remove version info from nginx and php

: Hiding versions doesn't fix weaknesses - but it helps not to advertise.

Https Headers

: Now on the TODO lost

I definitely want to set the secure flag on cookies

: These should never be available via plain http.

XSRF_TOKEN - httponly 

: It seems there is a [debate about this](https://security.stackexchange.com/questions/175536/does-a-csrf-cookie-need-to-be-httponly) but it is good to flag it and I can make a decision.

Laravel cookie

: This needs to be secure (also I should probably rename it)

info.php

: This is only on my local system and is .gitignored (I won't deploy it tp production)

## Summary

Nikto was very easy to install and with a single command dill a full scan of my website.

It found various missing headers that I can add to improve security.

It found the one bad file on the system - and reminds me that just because a file isn't linked doesn't make it secret.

This is just one small tool among many.