---
title : "Multiple Git Accounts"
date : 2024-07-29T11:12:14+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - git
 - Gitlab
 - Github

---

If you have multiple account with Gitlab or Github you will find that you can't use teh same ssh key for both accounts.

The nicest way I have found of configuring this is by putting the different projects in a subfolder - and then using a gitconfig conditional include

<!--more-->

I generate a second ssh key, specifying the filename  ~/.ssh/id_tangible

In the conditional include the sshCommand option tells git to use an additional parameter which specifies they key to use.

I also set a different email address for my git commits

```ini
#~/.gitconfig
[user]
        email = me@default
        name = Sean Burlington
[includeIf "gitdir:/home/sean/work/gitlab.com/tangiblebytes/"]
    path=.gitconfig-personal
```


```ini
#~/.gitconfig-personal
[user]
    email = me@tangible
[core]
    sshCommand = "ssh -i ~/.ssh/id_tangible"
```

Confirm it is working via

`git config --includes -l`

and it should tell you the values for the repo you are in 

Note that variables which are overridden wil be show twice - the later value is the one that is used.