---
title: "JavaScript Promise Reject vs Catch"
date: 2020-11-28T12:07:34Z
#images : ["/2016/10/image.jpg"]
description: "What's the difference between providing a rejection handler for a promise and catching errors ?"
#categories : ["category"]
draft: false
featured: true
author: Sean Burlington
tags:
  - JavaScript
  - Promises
---

What's the difference between providing a rejection handler for a promise and catching errors ?

And why Does the [React AJAX FAQ](https://reactjs.org/docs/faq-ajax.html) say

```Javascript
// Note: it's important to handle errors here
// instead of a catch() block so that we don't swallow
// exceptions from actual bugs in components.
```

<!--more-->

The difference between this

```Javascript
fetch("https://api.example.com/items")
      .then(res => res.json())
      .then(
        (result) => {
          // Do stuff
        },
        (error) => {
        // Handle Error
        }
      )
```

Compared with

```Javascript
fetch("https://api.example.com/items")
      .then(res => res.json())
      .then(
        (result) => {
          // Do stuff
        })
       .catch (
        (error) => {
        // Handle Error
        }
       );

```

The key thing is understanding that `then` [takes up to two arguments](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then)

```javascript
p.then(onFulfilled[, onRejected]);
```

The first is a function called if the promise is fulfilled, the second is called if it is rejected.

So these functions are not in a chain, which one is called just depends on if the current promise resolved, or was rejected.

Now let's look at catch.

The [catch method](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/catch) deals with rejection only.

> (in fact, calling obj.catch(onRejected) internally calls obj.then(undefined, onRejected)).

So now we can see that `promise().then().catch()` is different because it is a chain.

The catch statement with catch rejections thrown either by the original promise, or _within the then_

So: if your "Do stuff" code throws an error

```Javascript
fetch().then(do_stuff(), handle_error());
```

This will expose the error - which is good - you don't want silent errors in your code hidden by a handler that is just there in case of network glitches.

Whereas

```Javascript
fetch().then(do_stuff()).catch(handle_error();
```

Will catch errors from either the fetch failing **or** from something going wrong on do_stuff();

You may well want to catch errors but there is likely to be a different process for handling a fetch error vs handling an error that occurs writing your own code.

This is fairly basic Javascript - but as I'm coming here from a career of mostly PHP and Drupal coding I found I was getting confused having glossed over some nuances.
