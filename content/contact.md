---
title: "Contact"
date: 2020-06-05T14:36:34+01:00
#images : ["/2016/10/image.jpg"]
description: "Contact Page"
#categories : ["category"]
draft: false
author: Sean Burlington
---

My name is sean

This website is tangiblebytes.co.uk

put an @ between them and email me

Or use [linkedin.com/in/seanburlington](https://www.linkedin.com/in/seanburlington/)

My Apologies for not putting a mailto link or a contact form here - unfortunately these get abused by spammers to the point where I would likely not see any genuine message.
