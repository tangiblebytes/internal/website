---
title : "Vitest Partial Mock InertiaJs"
date : 2025-01-25T22:09:10Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - JavaScript
 - Testing
 - Vite
 - Vitest
 - InertiaJS
 - React

---

I have a Laravel project using vite InertiaJS and React 

A unit test calling the Head module from @inertiajs/react

Was triggering this error

`Cannot read properties of undefined (reading 'createProvider')`

For this test I don't really care about the Head module - but didn't want to try and mock the whole of inertia-react

<!--more-->

I order to partially mock I did this

```jsx
vi.mock('@inertiajs/react', async () => {
    const originalModule = await vi.importActual('@inertiajs/react')

    return { ...originalModule, Head: ({ title }) => <div data-testid="head">{title}</div> }
})
```

see https://vitest.dev/api/vi#vi-importactual 

Now my test works 🎉