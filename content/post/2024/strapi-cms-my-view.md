---
title : "Strapi CMS : My View"
date : 2024-01-31T11:19:44Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Strapi
 - CMS

---

I was asked to build a site using [Strapi CMS][1] and got part way down implementing it before concluding that this wasn't going to work.

These are the reasons I decided against continuing with Strapi.

<!--more-->

## Background

Strapi bills itself as

>  a flexible CMS whose admin panel and API are extensible - and which every part is customizable to match any use case.
>  
> [Strapi Welcome docs][2]

I am an experienced backend developer, comfortable with databases, APIs, and UI customisation - and Strapi came well recommended - it seemed like a good fit.

## The Good

It was fairly easy to create a basic content type with fields like title, subtitle, body

It has an API and managing permissions for which parts of it are public is straightforward.

The API allows front end devs to [specify how much data is populated][3] 

## Problems begin

Adding a rich text editor required a plugin which was fine but customising the editor looked like it was going to be tricky

The [components and dynamic zones][4] feature was quite tricky to use and I found it really hard to understand how the data model related to what I was seeing in the UI - as a backend developer modeling data through a basic web UI was not the way I like to do things. 

I also found the UI for editors really clunky - when adding things in dynamic zones new components appear off-screen and teh suer has to scroll them into view. 

There doesn't seem to be a way to do things like show preview thumbnails of youtube videos when a user adds a video component,

There is no direct taxonomy support in Strapi and what we did was create a few tag content types and related them to pages but there is [no parent/child relationship][5] so this is quite limited. There  no way to add a tag during the page edit flow - and you can't change the display of a tag.

A mobile editing UI wasn't a priority for this project - but I'm amazed to see a website these days that just doesn't work in mobile view.

## Stability Issues

Over a couple of months we saw many releases of Strapi, sometimes these fixed bugs we needed addressed, but sometimes they introduced new bugs and we had to roll back.

While frequent releases can be a good thing - it's hard when bugfixes and new features are rolled out together. 


## Multisite

My project has 9 websites which all have the same structure - we want one CMS that manages them all - we could spin up 9 versions but that is a lot of extra hosting costs and a lot more work for me.

Sadly I just couldn't see any way to make Strapi work for multiple sites - there is no way to filter content so that - for example when you enter a tag for siteA you only see the tags for that site.

## Development Experience

I found this to be the worst project to develop on of anything I have tried in my career - and I've worked on some gnarly projects.

Every change to code seems to require a server restart which is painfully slow.


Strapi is part way through a conversion from Javascript to Typescript so example code is often confusing with a wide variety of styles on display. A lof of the types are just defined as {any}. I should add here that JavaScript isn't my strongest language so I found this extra confusing.

Even development builds minify everything so that error messages are very hard to interpret and based on the compiled JavaSCrip[t not source Typescript  - if you get any error message. Often I'd just get a blank page.

A lot of data structures are undocumented so I ended up having to work by reverse engineering or trial and error - this all adds up to a really slow and frustrating build process.

The Strapi developer team are helpful and available over discord - but it's a very small community and has built up a much smaller set of documentation over time than other projects.

## Limited Customisation

While you can work on Models, and Controllers - the only View you can really affect in Strapi is the API 

The user interface is strongly Strapi branded and you [cant change it without hacking the core code][6].

Even with a plugin [you can only add content in 4 places][7]

## Going beyond "Content"

Often the system that you use to manage content has to do other jobs as well - like running a poll or a survey and generating reports on these. You might want to sell a product, or access to content, or record progress though a learning system.

Strapi models content but doesn't seem at all a good fit for other data - especially transactional data. You want something that lets you model, manage and store data for these things - but if you create a content type for say poll responses in Strapi - you also get an admin UI as if you wanted to be able to edit those responses. 

I managed to create a plugin for Strapi that managed a poll and linked though to a display of results - but it was a lot of work requiring the development of very Strapi-specific knowledge.

I don't think I could create a typical site management dashboard in Strapi to provide controls for and reports on these kids of site interactivity. 

If I tried to build a separate system for interactivity and reporting sitting across Strapi and the frontend website I think it would be very difficult. Strapi expects to have control over its database so I would be forced to use a second database and then have to sync data between systems. This would be a big increase in complexity and introduce the scope for many new types of error where the three parts of the system don't align or are out of sync. 


## The Myth of "Off the shelf"

People have been trying to commoditise CMS' since they were invented 

To some extent Wordpress and wix have succeeded 

But if you don't use a tool that has achieved this scale you are in some ways just using someone else's bespoke project.

Sure it may have some flexibility - but if you are doing custom development I think you are better off using a framework that promotes industry standard best practices and well documented frameworks.

Using a tool that gets you the first half of the way to a working site quickly is of no help if it makes the other half take 10 times as long, results in a worse product, and it is going to be really hard to find someone to support if it the team changes.

## Summary 

If you don't have a backend developer who can create a basic database with related [CRUD][8] operations, you don't care about the CMS UI, and you don't have complex requirements then maybe Strapi could work for you.

It is one of the view JavaScript CMS solutions available - but honestly the development experience is so bad I don't see that the underlying language matters - if you need to write code for it: use something else.




[1]: https://strapi.io/
[2]: https://docs.strapi.io/dev-docs/intro
[3]: https://docs.strapi.io/dev-docs/api/rest/populate-select 
[4]: https://docs.strapi.io/user-docs/content-type-builder/configuring-fields-content-type#-dynamic-zones
[5]: https://feedback.strapi.io/customization/p/parent-child-relationshipfield
[6]: https://forum.strapi.io/t/customize-the-dashboard-welcome-page/939
[7]: https://docs.strapi.io/dev-docs/api/plugins/admin-panel-api#injection-zones-api
[8]: https://en.wikipedia.org/wiki/Create,_read,_update_and_delete