---
title : "Connect Strapi to Digital Ocean Managed Mysql"
date : 2023-10-31T17:02:59Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - mysql
 - strapi
 - Digital Ocean

---

Connecting [Strapi](https://strapi.io/) to a [Digital Ocean Managed Mysql database](https://www.digitalocean.com/products/managed-databases-mysql) required jumping through a couple of hoops

The managed database is available over SSL using a custom CA certificate

<!--more-->

In order to connect, Strapi has to have the following environment variables:

(expressed here in YAML because my system runs in kubernetes)


```yaml
DATABASE_SSL: "true"
DATABASE_SSL_CA: | 
  -----BEGIN CERTIFICATE-----
  lots of lines here
  -----END CERTIFICATE-----
```

In addition I had to edit the Digital Ocean Database user to use `Legacy - MySQL 5.x` password encryption as it seems the Strapi mysql client doesn't support the newer standard yet.

