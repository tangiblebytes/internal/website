---
title : "Yarn, Vscode, and Typescript"
date : 2023-09-22T14:32:31+01:00
#images : ["/2016/10/image.jpg"]
description : "Yarn, VSCode, Typescript Cannot find module"
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - JavaScript
 - TypeScript
 - VScode
 - Yarn
 - node_modules

---

The JavaScript ecosystem is a sea of choices and it seems to be all too easy to make an incompatible selection.

In my case I am using the Yarn package manager, the VSCode editor and TypeScript flavoured JS (I don't think it really counts as a different language)

Every import statement was triggering an error in VScode 

> Cannot find module '' or its corresponding type declarations

<!--more-->

There is a  [good page on stackoverflow](https://stackoverflow.com/questions/69238794/cannot-find-module-next-or-its-corresponding-type-declarations)

[Yarn docs explain how to configure the edito](https://yarnpkg.com/getting-started/editor-sdks)


But neither of these worked for me and I've been down too many rabbit holes lately - I just want an easy solution.

The root of the issue is that Yarn 3 uses [Plug'n'Play'](https://yarnpkg.com/features/pnp) which doesn't create the old node_modules folder but instead has a loader file with links to pakcges on disk (and somehow involving zip files)

Package management is hard - and it sounds like this solves some real problems but it was breaking my editor - the proposed solutions didn't work - and I like having node_modules around to look through.

So I have downgraded yarn, reset my .vscode/settings.json file and everything seems to work OK 

`yarn set version classic`

`yarn install`

now I have my node_modules back

Another option seems to be to configure yarn3 to use node_modules

But the version of yarn available by default in node docker images is yarn classic and all teh Dockerfiles I have seen just use this directly - I doin't see it getting upgraded. So I'm sticking with yarn classic until I have a reason not to.





