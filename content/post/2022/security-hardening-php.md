---
title : "Security Hardening Php"
date : 2022-12-23T17:34:14Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - PHP
 - Security

---
<div class="clearfix">

![](/php-200.png)
{.float-left}

Most of what you read about securing PHP is how to write secure code - and that is really important.

In addition it helps to setup PHP on the server for best security.

There is plenty we can do to harden the setup without hurting the functionality we need.

The more layers we have in our security setup the better.

</div>



<!--more-->

## php.ini

There are various guides to securing php.ini settings 

The common factors are

- reduce the information you give to attackers
- log more information for yourself
- disable dangerous functions that you don't need
- add strict limits that suit your application

PHP is written to be powerful and flexible - to be able to do all sorts of stuff - but much of it you don't need.

Like including PHP files from remote servers, or running shell commands.


### Read up on how to tighten your settings


[OWASP PHP Configuration Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/PHP_Configuration_Cheat_Sheet.html)


### Try this script to review your config

https://github.com/sektioneins/pcc


## Snuffleupagus

[Snuffleupagus](https://snuffleupagus.readthedocs.io/) adds a whole other level of PHP hardening - it is the successor to [https://suhosin.org/](suhosin)


> Snuffleupagus has a lot of features that can be divided in two main categories: bug-classes killers and virtual-patching. The first category provides primitives to kill various bug families (like arbitrary code execution via unserialize for example) or raise the cost of exploitation. The second category is a highly configurable system to patch functions in php itself.

