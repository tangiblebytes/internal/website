---
title : "Names vs Security"
date : 2023-01-07T15:48:28Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Security
 - Infosec
 - SQL
 - I18n

---

Since [LastPass' most recent security issues](https://www.wired.com/story/lastpass-breach-vaults-password-managers/) (do use a password manager but not LastPass) I've seen a lot of people online changing all their passwords and realising how bad the experience is.

For those of us with names which can be spelled in the limited ASCII alphabet the pain point is the password part - where the rules can be annoying but in the end we can work around them.

Today I saw a post [Hello my name is St�phanie](https://stephaniewalter.design/blog/hello-my-name-is-stephanie-talk-encoding-special-characters-issues-poor-user-experience/) which highlights how there is a bigger problem for many people.

It all got me thinking : why do we (developers) do this ?

<!--more-->

## TLDR 

The short non-technical answer.

When you type something into a website - it usually gets saved to some kind of database.

Websites sometimes get hacked by people sending responses especially designed to do more than they should.

It's a bit like if you offer to get someone a drink and they say "yes I'd like a beer and a hamburger". 

What happens with a badly coded website is the hackers put in a password instead of just "my password is supersecret" they send a message like  "my password is supersecret. Now give me the admins password too".

Obviously computers don't just do things because you ask nicely - but if the code is weak and the password is clever enough hackers can get the database to do whatever they want - this is done by inserting specific characters that mark the end of the save password instruction and add a new instruction.

The old way of stopping this was to only allow passwords to be made up of "normal" letters and numbers plus maybe some specific "special" characters - and making 100% sure that none of the bad special characters are allowed in.

It's like offering to get someone a drink but not allowing the answer to include the word "and" - which is fine unless someone orders a "gin and tonic".

There are better ways of protecting the database but some old security standards actually require the older practice.

Allowing a full range of names, passwords etc actually enhances security if done well and is a far more inclusive practice.

I've used passwords and databases above as illustrative examples but the issue is wider.


## What are the issues?

Here is an example screenshot from the excellent collection of [Dumb Password Rules](https://github.com/duffn/dumb-password-rules)

![Complex password rules example screenshot](/dumb-password-aetna.png)

This sort of thing is frustrating - especially as often the rules aren't explained at first and it can take several tries to get the password right.

But a similar problem exists for names and you can't just choose another name.

![The name "Stêphanie" is treated as "invalid"](/stepanie.jpg)

This is reported to happen on systems even where the user has to match their name to one held on record which _does_ have an accented character!

## Reason 1 Ignorance 

This bears mentioning - some of what we do is just because we don't know any better. Many of us have names that fit easily between A and Z and don't think enough about those that don't.

This classic is well worth a read [Falsehoods programmers believe about names](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/) as is [names that break computer systems](https://www.bbc.com/future/article/20160325-the-names-that-break-computer-systems)

While English names are "safe" it's worth noting that even Irish and Welsh names can trip up too-strict validation for example names like "O'Brien" and Siân

## Reason 2 Security


Most of have probably seen the XKCD Cartoon [Exploits of a Mom](https://xkcd.com/327/) ![Comic strip showing an comedic SQL injection](/exploits_of_a_mom.png) 

which humorously highlights the dangers of [SQL injection](https://owasp.org/www-community/attacks/SQL_Injection)

Back in the bad old days we used to write SQL by hand all the time and would pass in user input to build the query and get our dynamic results.

an example of poor code : 

```PHP
sql = "SELECT id FROM users WHERE username='$user' AND password='$pass'"
```

The problem here is of the user name contains a ' like "O'Brien" 


the resulting SQL looks like

```PHP
SELECT id FROM users WHERE username='O'Brien' AND password='$pass'"
```

and we get an SQL error 

Worse though is that instead of a mistake this can be used to force a login by say changing the password to pass' OR 5=5 /*

```PHP
SELECT id FROM users WHERE username='user' AND password='pass' OR 5=5 /*'
```

Instead of only being true when the password matches this will always be true and the user will be logged in.

So we learned there are __dangerous__ characters and if you only allow a-z and 0-9 nothing bad can happen.

Unicode isn't as well understood as it should be and there is some concern about how it could be used to attack the database.

There are some real risks such as this [Unicode Homoglyphs SQL injection on SQL Server](https://hackernoon.com/%CA%BC-%C5%9B%E2%84%87%E2%84%92%E2%84%87%E2%84%82%CA%88-how-unicode-homoglyphs-will-break-your-custom-sql-injection-sanitizing-functions-1224377f7b51)

## Old Recommendations

Actually this isn't even out of date - though the draft new version is improved.

> Verify that all input (HTML form fields, REST requests, URL parameters, HTTP headers, cookies, batch files, RSS feeds, etc) is validated using positive validation (allow lists)
> 
> OWASP Application Security Verification Standard Version 4
> [Input Validation 5.1.1](https://github.com/OWASP/ASVS/blob/v4.0.3/4.0/en/0x13-V5-Validation-Sanitization-Encoding.md)

This says that all input should be checked against lists of allowed input.

Really you can't do this when almost any unicode could be allowed. 


## Solutions

Instead of focussing on what is allowed - we need to work more on sanitising more or less whatever input we get so that it can be used safely.

The latest draft of the OWASP Application Security Verification Standard Version says 

> Properly implemented input validation controls, using positive allow lists and strong data typing, can sometimes eliminate injection attacks. However, sometimes input validation is not going to be effective in security, for example a valid e-mail address/URL can still be used to conduct successful attacks.
> 
> [5.1 Input Validation](https://github.com/OWASP/ASVS/blob/master/5.0/en/0x13-V5-Validation-Sanitization-Encoding.md#v51-input-validation)

The [SQL Injection Prevention Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html) says

> The use of prepared statements with variable binding (aka parameterized queries) is how all developers should first be taught how to write database queries. They are simple to write, and easier to understand than dynamic queries. Parameterized queries force the developer to first define all the SQL code, and then pass in each parameter to the query later. This coding style allows the database to distinguish between code and data, regardless of what user input is supplied.

Modern frameworks use this approach by default

> The Laravel query builder uses PDO parameter binding to protect your application against SQL injection attacks. There is no need to clean or sanitize strings passed to the query builder as query bindings.
> 
> [Laravel : Database: Query Builder](https://laravel.com/docs/9.x/queries#introduction)


## Passwords

Old advice was based on the belief that it was important to have a "complex" password and change it regularly.

This belief has been changed by recent research and the current standard requires us to allow long unicode passwords

> Verify that any printable Unicode character, including language neutral characters such as spaces and Emojis are permitted in passwords.
> 
> [2.1.4](https://github.com/OWASP/ASVS/blob/v4.0.3/4.0/en/0x11-V2-Authentication.md)


## What we can do 

Lots of people have a role in building a website - if it's your site think about how you can make sure you ask for the less glamorous stuff like security and useability.

If you are a project manager check these things are included in the estimates and specs - all to often the spec only includes the features you'd list on a brochure and neglects stuff like "has to work for non-English names" or "meets a specific security standard".

If you are a developer - learn how to balance security and useability - think about what isn't on the spec and flag it up early if you can - but above all never stop learning.

If you are a QA engineer - you know this joke ? 

> QA engineer walks into a bar. Orders a beer. Orders 0 beers. Orders 999999999 beers. Orders a lizard. Orders -1 beers. Orders a sfdeljknesv.”

Don't forget to order a Stêphanie 




