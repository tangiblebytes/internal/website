---
title : "Infosec for Web Developers"
date : 2022-12-28T11:34:07Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Security
 - OWASP


---
<div class="clearfix">
The web is not secure enough. Every day we hear stories of sites being hacked, businesses and lives ruined.

As an industry we have to do better.

First we need to study common issues, security standards and processes.

It also helps to try a little ethical hacking to see things from the other side.


</div>
<!--more-->

## Start here  

We should all be familiar with the OWASP top ten which is a great starting point for being aware of the most common issues.

- https://owasp.org/www-project-top-ten/

Every developer needs to read this list on an annual basis - and make sure there aren't any surprises there.

## Detailed Rules

OWASP Application Security Verification Standard

- https://owasp.org/www-project-application-security-verification-standard/

Even if your company doesn't use this standard - I think it's a great place to learn what good practice looks like.

If you are a security conscious developer you will likely run into situations where you run into conflict with someone who wants to meet a business need or a deadline - in which case it really helps everyone oif you can supply a strong reference and not just your opinion.

That way the business can make an informed decision (which won't always be to do the secure thing)

As an example this document has requirement 2.1.5 "Verify users can change their password" (which is in level 1 of the standard) 

It references [CWE 620](https://cwe.mitre.org/data/definitions/620.html) and [NIST 5.1.1.2](https://pages.nist.gov/800-63-3/sp800-63b.html#-5112-memorized-secret-verifiers)

This makes for a much easier conversation about adding a security feature.

## Going Further 

To really build secure apps - you need security built into the heart of the development process 

[OWASP Software Assurance Maturity Model (SAMM)](https://owaspsamm.org/)

> The mission of OWASP Software Assurance Maturity Model (SAMM) is to be the prime maturity model for software assurance that provides an effective and measurable way for all types of organizations to analyze and improve their software security posture.

You also need to test for security 

[OWASP Web Security Testing Guide](https://owasp.org/www-project-web-security-testing-guide/latest/)

But this is really going into the realms of teams and managers - something for individual developers to be aware of and to support, but not something you can really do on your own.




