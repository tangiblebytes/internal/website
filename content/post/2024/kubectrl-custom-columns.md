---
title : "Kubectrl Custom Columns"
date : 2024-08-07T17:35:22+01:00
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Kubernetes
 - kubectl
 - Jsonpath

---
<div class="clearfix">

![](/kubernetes.png)
{.float-left}

Kubernetes provides a wide variety of methods for formatting output 

The custom columns output lets you print multiple values defined using jsonpath expressions in a tabular output  

While outputting scalar values is reasonably intuitive, I couldn't see in the docs where is says what the format for defining custom-columns is.


</div>
<!--more-->


> -o custom-columns=&lt;spec&gt;	Print a table using a comma separated list of custom columns.

Once I understood it was jsonpath, accessing something like an array of hosts from my ingresses worked nicely

```bash
kubectl get -A ingresses -o custom-columns=\
Hosts:.spec.rules[*].host,Name:.metadata.name,NameSpace:.metadata.namespace
```


## Links

 * [kubectl Custom Columns](https://kubernetes.io/docs/reference/kubectl/#custom-columns)
 * [Kubectl JSONPath Support](https://kubernetes.io/docs/reference/kubectl/jsonpath/)
 * [Guide to kubectl Output Formatting](https://www.baeldung.com/ops/kubectl-output-format)