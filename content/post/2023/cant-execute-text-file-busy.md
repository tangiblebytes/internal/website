---
title : "env: can’t execute ‘node’: Text file busy"
date : 2023-10-31T14:05:27Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - docker
 - kubernetes
 - NodeJS

---

In Kubernetes my pod was failing with

`env: can’t execute ‘node’: Text file busy`

It worked fine locally using docker

<!--more-->

Searches suggested that this was a bug specific to node 20 but I am using node 18

I don't understand why it occurred only in kubernetes but not in docker (I tried different build and had the same problem)

Upgrading from node:18.18.0-alpine3.18 to node:18.18.2-alpine3.18 resolved the issue


