# Hugo Sources [tangiblebytes.co.uk](tangiblebytes.co.uk)

This is the website for my small company - I use it to blog about things I have learned, and place reminders for my future self.

## Technologies 

* hugo
* bootstrap
* firebase
* workbox
* PWA
* CSP


## TODO / Status

PWA caching seems to be to severe and PWA apps don't get new content promptly - I need to investigate workbox properly to fix this
https://developers.google.com/web/tools/workbox/guides/get-started

I _think_ CSP is implemented correctly - logging at sentry.io

publish via ./publish.sh

staging builds a local site without draft pages and serves via firebase emulators
functions contains a commented out mail handler - I decided this resulted in too much spam and a twitter link would be sufficient 

## Use

### new content 

`hugo new content content/post/2025/nextjs-env-vars-docker.md`

Remember to edit the metadata

## New Year

Change the new content example to the right year

add a new directory such as content/post/2025

Copy the _index.md from a previous year and change the dates

Edit themes/hugo-theme-bootstrap4-blog/layouts/partials/recent.html

add a link to the new year

### preview

`hugo -D serve`

### publish 

**Edit the draft status to false** then

`publish.sh`



## build dependencies

This seems to need doing each time I switch JS versions 

```bash
npm install -g firebase-tools
npm install -g workbox-cli
```

When moving to a new dev machine

`apt install hugo`

```bash
cd themes/hugo-theme-bootstrap4-blog/
npm install
```

`firebase login` (practicalweb account)

on practicalweb account
