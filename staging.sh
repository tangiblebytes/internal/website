#!/bin/bash -ex

cd $(dirname $0);

hugo -b http://localhost:5000/ --cleanDestinationDir  --minify
workbox generateSW ./workbox-config.js
firebase emulators:start