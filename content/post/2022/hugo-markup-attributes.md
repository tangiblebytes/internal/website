---
title : "Hugo Markup Attributes"
date : 2022-11-16T12:50:02Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Hugo
 - Bootstrap
 - Markdown

---
<div class="clearfix">

![](/hugoSM.png)
{.float-left}

I have been blogging since about 2005 and been through several different platforms in the process

Using a Static Site Generator with content in Markdown is awesome for low maintenance and content portability.

If I move platform it shouldn't be hard to migrate the content and to that end I want to keep the my posts fairly pure Markdown - but it can be a bit limited.

How do I add some classes to change how it looks?

</div>
<!--more-->

 


> Hugo supports adding attributes (e.g. CSS classes) to Markdown blocks, e.g. tables, lists, paragraphs etc.

I use [configure markup](https://gohugo.io/getting-started/configuration-markup/) to add class attributes and combine it with [Bootstrap utility classes](https://getbootstrap.com/docs/4.0/utilities/clearfix/)

I also allow myself to write a little html in my content

```toml
[markup]
  [markup.goldmark.renderer]
      unsafe = true
```

Then in my default archetype I have 


```markdown
---
<div class="clearfix">

![](/default.png)
{.float-left}


</div>
<!--more-->
```

Hugo renders my image as 

```html
<p class="float-left"><img src="/default.png" alt=""></p>
```

and leaves the wrapping html intact

The combination of being able to add a class and Bootstrap utilities is quite powerful.

I'd like to find a way to avoid the HTML in my content - and wrap the into automatically.
 
Also worth noting is Hugo's [render hooks](https://gohugo.io/templates/render-hooks/)