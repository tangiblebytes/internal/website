---
title: "Ubuntu Installer Blank Screen"
date: 2021-01-12T15:25:43Z
#images : ["/2016/10/image.jpg"]
description: ""
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - linux
---

I was recently updating a very old laptop for use in some network testing - all it needs to do is connect to wifi.

I couldn't understand why I was getting a blank screen after grub loaded

I tried various install media, tried editing grub conf.

The best I could get was a screen saying

`Booting a command list`

<!--more-->

This sticky post from ubuntu forums [Graphics Resolution- Upgrade /Blank Screen after reboot](https://ubuntuforums.org/showthread.php?t=1743535) sent me in the right direction and I realised the kernel wasn't starting at all.

Then the penny dropped and I realised this very old laptop is 32 bit.

It won't run any recent version of ubuntu.

But it will run 32 bit [debian](https://www.debian.org/distrib/).

It's never going to be fast but it should work for my purposes.
