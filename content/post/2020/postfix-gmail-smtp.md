---
title: "Postfix via Gmail SMTP"
date: 2020-10-26T19:12:30Z
#images : ["/2016/10/image.jpg"]
description: ""
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - smtp
  - postfix
  - cron
  - ubuntu
---

I've been taking a bit of time lately to do some of those low priority housekeeping tasks that can easily get overlooked.

By using quiet times to setup good processes life should be easier next time things get busy.

One of those tasks was to setup outgoing email on various systems - some of which are laptops and I'd still like automated mails to go out even if the device isn't on the home network and can't reach my ISP mailserver.

Back in the day you'd just send mail directly - but with spam protection everywhere this just doesn't work and it's far better to authenticate to some trusted mailserver.

I'm using gmail so I'll use their servers.

<!--more-->

## First Setup your Google Account

I created an account just for this purpose - email is sent from here and a copy will be stored in the sent folder of this account.

By using a separate account I avoid granting any machine access to my main account.

- Login to [your account](https://myaccount.google.com/)
- Go to the [Security tab](https://myaccount.google.com/security)
- Make sure you have 2 factor authentication enabled enabled
- Create an [app specific password](https://myaccount.google.com/apppasswords)

Note that you _can_ opt to enable access for "less secure apps" and use your main password to authenticate.

The app specific password has three advantages

1.  it can't be used to login normally - and so can't be used to change the password and steal the account
1.  access is limited to the service you grant access to (eg email - but not docs)
1.  you can have lots of app passwords and can revoke them individually (so if one gets compromised you just revoke that one)

## Now install and configure Postfix

I'm on Ubuntu 20.04

`sudo apt-get install libsasl2-modules postfix`

Specify smarthost in the configure step

If you want to reconfigure run `sudo dpkg-reconfigure postfix`

add these lines to your postfix config /etc/postfix/main.cf (or edit existing values)

```
relayhost = [smtp.gmail.com]:587

smtp_sasl_auth_enable = yes
smtp_sasl_security_options =
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
```

create / edit /etc/postfix/sasl_passwd as below

`[smtp.gmail.com]:587 bot@myemail.co.uk:myappspecificpassword`

run
`postmap /etc/postfix/sasl_passwd`

`sudo service postfix reload`

Now send a test email like

`echo test | mail -s test $USER`

and look in `/var/log/mail.log`

you should see mail sent via smtp.gmail.com - or an error message if there is a problem.

I then setup a slightly older system running Ubuntu 19.10 (which I need to update)

This time I got an authentication failure and as it was the exact same setup I'd just got working I searched around and it seems to be a [bug](https://bugzilla.redhat.com/show_bug.cgi?id=1575424) in that version of Ubuntu.

The workaround is to add to /etc/postfix/main.cf

`smtp_sasl_mechanism_filter = login`
