---
title: "Firebase Emulator Add Users"
date: 2020-11-26T15:23:04Z
#images : ["/2016/10/image.jpg"]
description: "Automatically add users to the Firebase Authentication Emulator"
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - Firebase
  - Emulator
---

I've been using [Google's Firebase](https://firebase.google.com/) recently and find it a great development platform.

One of the best things about it is the emulators available for local development.

Just a few weeks ago they made it even better by adding an [authentication emulator](https://firebase.google.com/docs/emulator-suite/connect_auth)

So now you can easily start up a clean environment to test your new code on.

But the auth emulator starts with no users every time.

This is one way to add users.

<!--more-->

- Add the following to your functions/index.js file
- Adjust the users section as needed.
- When you start the emulator request the page
- http://localhost:5001/${your_project}/us-central1/populateAuthUsers

```JavaScript

exports.populateAuthUsers = functions.https.onRequest(async (req, res) => {
  if (!process.env["FUNCTIONS_EMULATOR"]) {
    return res
      .status(403)
      .send("ACCESS DENIED. This function is ONLY available via an emulator");
  }
  const users = [
    {
      uid: "user1",
      displayName: "one Local Admin",
      email: "one@test.com",
      password: "password",
    },
    {
      uid: "user2",
      displayName: "two Local Admin",
      email: "two@test.com",
      password: "password",
    },
    // put all test users you want populated here
  ];

  const results = [];
  const promises = [];
  for (let user of users) {
    let promise = admin
      .auth()
      .createUser(user)
      .then((result) => {
        return result;
      })
      .catch((error) => {
        return error.message; // continue on errors (eg duplicate users)
      });


```

## Firestore data

If you need firestore data as well just export what you need

`firebase emulators:export ./emulator-data/`

and start the emulator with an import

`firebase emulators:start --import=./emulator-data/`

Hopefully this will include auth data too in the future

## References

- https://stackoverflow.com/questions/64771219/can-i-use-the-authimport-and-authexport-tools-with-the-firebase-auth-emulator/64972897
- https://github.com/firebase/firebase-tools/issues/2749
