---
title : "Jwt Secret"
date : 2023-10-30T11:08:55Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - jwt
 - security
 - strapi

---
[JWT](https://jwt.io/) uses secrets which are stored securely

This secret is used to sign (opr encrypt) the token - and then the token can be validated using this secret. If the secret is shared between multiple servers - the token can ba validated on any of them.

The secret can change over time (at which point any previous tokens expire) and should be different in each environment eg there is no need to have the same secret in staging and production as token should not be valid across these.

It hasn't been especially clear to me how a new token is best generated.


 <!--more-->

I'm using [strapi](https://strapi.io/) which generates token for a new install like so 

```javascript
const generateASecret = () => crypto_1.default.randomBytes(16).toString('base64');
```

And as far as I can tell the JWT secret should be a randomly generated, base64 encoded, string 

The length of the string seems to be a balance where longer is more secure but more computationally expensive.

Recommendations seem to vary between a length of 16 and 64 

To generate a 32 char secret via bash

```bash
openssl rand -base64 32
```


