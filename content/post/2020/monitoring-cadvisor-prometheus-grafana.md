---
title: "System Monitoring with Cadvisor, Prometheus, and Grafana"
date: 2020-04-22T21:15:36+01:00
draft: true

tags: ["grafana", "cadvisor", "prometheus", "linux", "monitoring"]
---

apt install cadvisor prometheus

<!--more-->

https://bugs.launchpad.net/ubuntu/+source/cadvisor/+bug/1874323?comments=all

Download cadvisor and replace binary

configure prometheus to scrpae cadvisor

scrape_configs:

- job_name: cadvisor
  scrape_interval: 5s
  static_configs:
  - targets:
    - localhost:4194

Download grafana

Add dashboards

https://grafana.com/grafana/dashboards/11074
