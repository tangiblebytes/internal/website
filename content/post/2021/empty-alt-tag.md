---
title: "Empty Alt Tag"
date: 2021-02-08T09:21:08Z
#images : ["/2016/10/image.jpg"]
description: ""
#categories : ["category"]
draft: false
author: Sean Burlington
tags:
  - Accessibility
---

Part of what makes the web so brilliant is in it's flexibility - the information is conveyed mostly in digital text and unlike paper we don't all have to experience that in the same way.

Some people can't see the images in a website so we can add "alt" text which replaces the image for these people.

The problem comes when the image doesn't actually have a meaning - then what should the Alternative text be?

<!--more-->

There is a myth that the alt text should always say something.

In fact some images should have empty alt text - as this conveys the correct meaning - which is that the image is purely decorative and need not be considered to interpret the meaning of the page.

This is documented in the [techniques and failures for web content accessibility guidelines][wcag] document under ["using null alt text and no title attribute on img elements for images that at should ignore"][h67]

> If no title attribute is used, and the alt text is set to null (i.e. alt="") it indicates to assistive technology that the image can be safely ignored.

[h67]: https://www.w3.org/TR/WCAG20-TECHS/html.html#H67
[wcag]: https://www.w3.org/TR/2016/NOTE-WCAG20-TECHS-20161007/
