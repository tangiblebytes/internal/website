---
title: "Hugo Shortcode"
date: 2020-06-04T11:40:38+01:00
draft: false
tags:
  - hugo
---

I wanted to use an html `<aside>` block in my blog and this isn't supported by Markdown

It's fairly easy to implement as a hugo shortcode though

I created a file (path from the root of my hugo site)

`layouts/shortcodes/aside.html`

<!--more-->

```html
<aside>
  {{/* .Inner | markdownify */}}
</aside>
```

Then in my markdown content

```markdown
{{</* aside */>}}

This is an aside

{{</* /aside */>}}
```

and with the help of some CSS it looks like this (with an actual aside as I go down another rabbithole)

{{< aside >}}

### How to escape shortcodes in Markdown

Above I want to show hugo shortcodes in my output

But these are processed before markdown so I can't escape them with markdown formatting

The secret is to add comment marks which stop the shortcode being executed and are removed before display

https://liatas.com/posts/escaping-hugo-shortcodes/

```markdown
{{</*/* aside */*/>}}

This is an aside

{{</*/* /aside */*/>}}
```

{{< /aside >}}
