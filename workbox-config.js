module.exports = {
  globDirectory: "public/",
  globPatterns: ["**/*.{css,ico}", "appicons/*"],
  swDest: "public/sw.js",
  directoryIndex: "index.html",
};
