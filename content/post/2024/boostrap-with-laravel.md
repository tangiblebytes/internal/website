---
title : "Boostrap With Laravel"
date : 2024-02-12T11:31:15Z
#images : ["/2016/10/image.jpg"]
description : ""
#categories : ["category"]
draft : false
author : Sean Burlington
tags :
 - Laravel
 - Inertia
 - Bootstrap
 - React
 - Tailwind

---
<div class="clearfix">

![](/laravel-logo-200.png)
{.float-left}

As a backend developer I'm a big fan of [Bootstrap](https://getbootstrap.com/) it is a framework that lets me quickly put together user interfaces with consistent components using Modals, Alerts, Toasts and more - things that I don't want to have to learn how to make work and add design to.

It seems like Laravel used to use Bootstrap by default but has migrated to [Tailwind CSS](https://tailwindcss.com/) 


</div>
<!--more-->

## I don't get Tailwind

I learned web development before CSS arrived - the only wy to add style was to split the page up into tables and defined colours and backround images for each cell - mixing markup and design like this was a nightmare making any changes very hard to manage.

Tailwind feels to me like a step back in this direction.

What should be a simple welcome message looks like 

```html
<div className="py-12">
    <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
           <div className="p-6 text-gray-900">You're logged in!</div>
        </div>
    </div>
</div>
```

I'm much much happier with 

```html
<div class="welcome">You're logged in!</div>
```

## Components 

I'm not doing complex UI design - generally the Bootstrap components do what I want - I don't want to have to build my own - and Tailwind is only CSS - it doesn't provide components.

[Headless UI](https://headlessui.com/) - provides some but isn't as rich.

## Switching Laravel to use Bootstrap

`npm install react-bootstrap bootstrap`

Edit resources/js/app.jsx and add this imports

```javascript
import 'bootstrap/dist/css/bootstrap.min.css';
```

Don't get confused by the existing line `import './bootstrap';` this refers to internal Laravel bootstrapping code (leave it there)

Now you can start removing any tailwind code and replacing it with Bootstrap components

(NB I'm writing thi8s up a bit after I did it - hopefully I didn't forget a step - this is what I think I did)


## InertiaJS

One issue I had is tha the default Bootstrap Nav items were triggering full page reloads and not getting intercepted by the Inertia router


> To create links to other pages within an Inertia app, you will typically use the Inertia <Link> component. This component is a light wrapper around a standard anchor <a> link that intercepts click events and prevents full page reloads. This is how Inertia provides a single-page app experience once your application has been loaded.
>
> https://inertiajs.com/links

To address this at first I simply used <Link> elements in my nav and added classnames so that they pick up the Bootstrap styles.

```jsx
<Nav >
    <Link class="nav-link"  href="/articles">
         linktext    
    </Link>
</Nav>
```

and for a dropdown

```jsx
<NavDropdown title={user.name} id="basic-nav-dropdown">
    <NavDropdown.Item href={route('profile.edit')}>Profile</NavDropdown.Item>
    <Link href={route('logout')} class="dropdown-item" method="post" as="button" type="button">Logout</Link>
</NavDropdown>
```


But this felt hacky and was more code

## onClick Solution

I realised the `<link>` element isn't that special it is just a regular link with an onClick handler that uses the inertia router 

So I as well as using the href property of my Pagination Items I can use an onClick handler the [inertia router](https://inertiajs.com/manual-visits)

```jsx
<Pagination>
    {pages.links.map(link => {
        return (
                    <Pagination.Item 
                        active={link.active} 
                        href={link.url} 
                        onClick={() => router.visit(link.url) } 
                        key={link.label}
                        > {link.label}</Pagination.Item>
                )
    })}
</Pagination>
```


You can do the same thing with named routes


```jsx
<Nav variant="tabs">
  <Nav.Item>
    <Nav.Link
      active={activeTab == "view"}
      href={route("category.show", {site: site.id, category: category.id})}
      onClick={() =>
        router.visit(
          route("category.show", {site: site.id, category: category.id})
        )
      }
    >
      View
    </Nav.Link>
  </Nav.Item>
</Nav>
```

## Conclusion 

Using Bootstrap in Laravel is easy

 1. npm install
 2. import bootstrap css
 3. use onClick handlers as well as href 